-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table dogom.accounts
CREATE TABLE IF NOT EXISTS `accounts` (
  `id_taikhoan` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tentaikhoan` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `matkhau` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quyen` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_taikhoan`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dogom.accounts: ~0 rows (approximately)
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` (`id_taikhoan`, `tentaikhoan`, `matkhau`, `quyen`, `created_at`, `updated_at`) VALUES
	(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, '2021-04-05 23:38:32', '2021-04-05 23:38:32'),
	(2, 'khachhang1', 'e10adc3949ba59abbe56e057f20f883e', 2, '2021-04-06 23:19:08', '2021-04-06 23:19:08');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;

-- Dumping structure for table dogom.banner
CREATE TABLE IF NOT EXISTS `banner` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tieude` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hinhanh` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trangthai` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dogom.banner: ~2 rows (approximately)
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` (`id`, `tieude`, `hinhanh`, `trangthai`, `created_at`, `updated_at`) VALUES
	(1, 'tieude', '3 (1).png', 1, '2021-04-13 21:35:07', '2021-04-13 21:35:07'),
	(2, 'tieude', '3.png', 1, '2021-04-13 21:52:44', '2021-04-13 21:52:44');
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;

-- Dumping structure for table dogom.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id_loaisanpham` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ten_loaisanpham` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_loaisanpham`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dogom.categories: ~0 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id_loaisanpham`, `ten_loaisanpham`, `created_at`, `updated_at`) VALUES
	(1, 'category 1', '2021-04-08 21:29:34', '2021-04-08 21:29:34');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table dogom.contact
CREATE TABLE IF NOT EXISTS `contact` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ten` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dienthoai` char(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diachi` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tieude` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noidung` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dogom.contact: ~0 rows (approximately)
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;

-- Dumping structure for table dogom.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `id_khachhang` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_taikhoan` bigint(20) unsigned NOT NULL,
  `ten_khachhang` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dienthoai` char(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diachi` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_khachhang`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dogom.customers: ~0 rows (approximately)
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`id_khachhang`, `id_taikhoan`, `ten_khachhang`, `email`, `dienthoai`, `diachi`, `created_at`, `updated_at`) VALUES
	(1, 2, 'Khách hàng', 'test@gmail.com', '0123456789', 'test', '2021-04-06 23:19:08', '2021-04-06 23:19:08');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- Dumping structure for table dogom.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dogom.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table dogom.feedback-detail
CREATE TABLE IF NOT EXISTS `feedback-detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_topic` int(11) NOT NULL,
  `id_taikhoan` int(11) NOT NULL,
  `noidung` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dogom.feedback-detail: ~0 rows (approximately)
/*!40000 ALTER TABLE `feedback-detail` DISABLE KEYS */;
INSERT INTO `feedback-detail` (`id`, `id_topic`, `id_taikhoan`, `noidung`, `created_at`, `updated_at`) VALUES
	(1, 1, 2, 'Nội dung', '2021-04-21 15:37:56', '2021-04-21 15:37:56'),
	(2, 1, 2, 'test', '2021-04-21 16:10:37', '2021-04-21 16:10:37'),
	(3, 1, 1, 'reply', '2021-04-21 17:07:08', '2021-04-21 17:07:08'),
	(4, 1, 1, 'reply', '2021-04-21 17:07:42', '2021-04-21 17:07:42'),
	(5, 1, 1, 'test', '2021-04-21 17:08:00', '2021-04-21 17:08:00');
/*!40000 ALTER TABLE `feedback-detail` ENABLE KEYS */;

-- Dumping structure for table dogom.feedback-topic
CREATE TABLE IF NOT EXISTS `feedback-topic` (
  `id_topic` int(11) NOT NULL AUTO_INCREMENT,
  `id_khachhang` int(11) DEFAULT '0',
  `chude` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_topic`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dogom.feedback-topic: ~0 rows (approximately)
/*!40000 ALTER TABLE `feedback-topic` DISABLE KEYS */;
INSERT INTO `feedback-topic` (`id_topic`, `id_khachhang`, `chude`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Vấn đề abc', '2021-04-21 15:37:56', '2021-04-21 17:08:00');
/*!40000 ALTER TABLE `feedback-topic` ENABLE KEYS */;

-- Dumping structure for table dogom.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dogom.migrations: ~12 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2019_08_19_000000_create_failed_jobs_table', 1),
	(3, '2021_03_29_210728_products', 1),
	(4, '2021_03_29_212041_categories', 1),
	(5, '2021_03_29_212223_news', 1),
	(6, '2021_03_29_220550_contact', 1),
	(7, '2021_03_29_225508_customers', 1),
	(8, '2021_03_29_234313_settings', 1),
	(9, '2021_03_29_234525_banner', 1),
	(10, '2021_03_30_155857_accounts', 1),
	(11, '2021_03_30_160133_orders', 1),
	(12, '2021_03_30_161906_order-detail', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table dogom.news
CREATE TABLE IF NOT EXISTS `news` (
  `id_tintuc` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ten_tintuc` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ma_tin` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hinhanh` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noidung` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tinhtrang` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_tintuc`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dogom.news: ~0 rows (approximately)
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` (`id_tintuc`, `ten_tintuc`, `ma_tin`, `hinhanh`, `noidung`, `tinhtrang`, `created_at`, `updated_at`) VALUES
	(1, 'Tin tức 1', 'tin-tuc1', '1c0d5bbf88a17668b6f23ab4d3243fee13dcdce4_full.jpg', '<p>Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;Nội dung&nbsp;</p>', 1, '2021-04-09 21:48:36', '2021-04-11 20:32:31');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;

-- Dumping structure for table dogom.order-detail
CREATE TABLE IF NOT EXISTS `order-detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_donhang` bigint(20) unsigned NOT NULL,
  `id_sanpham` bigint(20) unsigned NOT NULL,
  `soluong` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `gia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dogom.order-detail: ~0 rows (approximately)
/*!40000 ALTER TABLE `order-detail` DISABLE KEYS */;
INSERT INTO `order-detail` (`id`, `id_donhang`, `id_sanpham`, `soluong`, `created_at`, `updated_at`, `gia`) VALUES
	(1, 1, 1, 1, '2021-04-11 18:14:10', '2021-04-11 18:50:12', NULL),
	(2, 2, 2, 1, '2021-04-19 22:25:30', '2021-04-19 22:25:30', 1000);
/*!40000 ALTER TABLE `order-detail` ENABLE KEYS */;

-- Dumping structure for table dogom.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id_donhang` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_khachhang` bigint(20) unsigned NOT NULL,
  `trangthai` tinyint(4) NOT NULL COMMENT '1-Đang chờ\r\n2-Đang xử lý\r\n3-Vận chuyển\r\n4-Hoàn thành\r\n5-Đã hủy',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_donhang`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dogom.orders: ~0 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id_donhang`, `id_khachhang`, `trangthai`, `created_at`, `updated_at`) VALUES
	(1, 1, 4, '2021-04-11 18:13:46', '2021-04-11 18:50:44'),
	(2, 1, 4, '2021-04-19 22:25:30', '2021-04-19 22:49:26');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table dogom.products
CREATE TABLE IF NOT EXISTS `products` (
  `id_sanpham` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ten_sanpham` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ma_sanpham` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hinhanh` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `giadexuat` bigint(20) NOT NULL DEFAULT '0',
  `giamgia` bigint(20) DEFAULT NULL,
  `soluong` int(11) NOT NULL,
  `id_loaisp` bigint(20) unsigned NOT NULL,
  `tinhtrang` tinyint(4) NOT NULL,
  `noidung` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_sanpham`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dogom.products: ~0 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id_sanpham`, `ten_sanpham`, `ma_sanpham`, `hinhanh`, `giadexuat`, `giamgia`, `soluong`, `id_loaisp`, `tinhtrang`, `noidung`, `created_at`, `updated_at`) VALUES
	(1, 'Sản phẩm 1', 'sp1', '1c0d5bbf88a17668b6f23ab4d3243fee13dcdce4_full.jpg', 9999, 98, 10, 1, 1, '<p>enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;enctype=&quot;multipart/form-data&quot;</p>', '2021-04-08 21:55:55', '2021-04-21 18:29:06'),
	(2, 'Sản phẩm 2', 'sp2', '1.png', 1000, 0, 100, 1, 1, '<p>Test</p>', '2021-04-11 16:40:44', '2021-04-21 18:29:47');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table dogom.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tencuahang` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diachi` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dienthoai` char(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dogom.settings: ~0 rows (approximately)
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `created_at`, `updated_at`, `tencuahang`, `diachi`, `logo`, `dienthoai`, `email`, `facebook`) VALUES
	(1, '2021-04-09 21:04:49', '2021-04-09 21:04:49', 'Cửa hàng đồ gốm', 'Địa chỉ', '163899848_475268093832414_6254720937601776837_n.png', '0123456789', 'admin@gmail.com', 'facebook');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Dumping structure for table dogom.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dogom.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
