<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id_sanpham');
            $table->char('ten_sanpham', 255);
            $table->char('ma_sanpham', 255);
            $table->char('hinhanh', 255);
            $table->float('giadexuat');
            $table->float('giamgia')->nullable();
            $table->integer('soluong');
            $table->unsignedBigInteger('id_loaisp');
            $table->tinyInteger('tinhtrang');
            $table->longText('noidung');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
