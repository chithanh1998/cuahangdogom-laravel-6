<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback_detail extends Model
{
    protected $table = 'feedback-detail';
    protected $primaryKey = 'id';
}
