<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback_topic extends Model
{
    protected $table = 'feedback-topic';
    protected $primaryKey = 'id_topic';
}
