<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;
use App\Banner;
use App\Categories;
use App\Products;
use App\Orders;
use App\Order_detail;
use App\News;
use App\Accounts;
use App\Customers;
use App\Feedback_topic;
use App\Feedback_detail;
use View;

class FrontendController extends Controller
{
    public function __construct() {
        $caidat = Settings::first();
        $banner = Banner::all();
        View::share ( 'caidat', $caidat );
        View::share ( 'banner', $banner );
     }
    
    public function trangchu()
    {
        
        $danhmuc = Categories::all();
        $sanphammoi = Products::latest()->get()->take(8);
        $banchay = Order_detail::leftjoin('orders','order-detail.id_donhang','=','orders.id_donhang')->where('orders.trangthai', 4)->groupBy('id_sanpham')->selectRaw('sum(soluong) as sum, id_sanpham')->orderBy('sum', 'desc')->get()->take(8);
        $sanpham = Products::all();
        return view('frontend.index',['sanphammoi'=>$sanphammoi,'banchay'=>$banchay,'sanpham'=>$sanpham,'danhmuc'=>$danhmuc]);
    }

    public function danhmuc($id)
    {
        $danhmuc = Categories::all();
        $tendanhmuc = Categories::find($id)->ten_loaisanpham;
        $sanpham = Products::where('id_loaisp', $id)->latest()->paginate(12);
        return view('frontend.danhmuc',['sanpham'=>$sanpham,'tendanhmuc'=>$tendanhmuc,'danhmuc'=>$danhmuc]);
    }

    public function chitietsp($ma_sanpham)
    {
        $sanphammoi = Products::latest()->get()->take(5);
        $banchay = Order_detail::leftjoin('orders','order-detail.id_donhang','=','orders.id_donhang')->where('orders.trangthai', 4)->groupBy('id_sanpham')->selectRaw('sum(soluong) as sum, id_sanpham')->orderBy('sum', 'desc')->get()->take(5);
        $allsanpham = Products::all();
        $sanpham = Products::where('ma_sanpham', $ma_sanpham)->first();
        $danhmuc = Categories::where('id_loaisanpham', $sanpham->id_loaisp)->first();
        return view('frontend.chitiet',['sanphammoi'=>$sanphammoi,'sp'=>$sanpham,'sanpham'=>$allsanpham,'banchay'=>$banchay,'danhmuc'=>$danhmuc]);
    }

    public function get_dangnhap()
    {
        $id_khachhang = session()->get('id_khachhang');
        if($id_khachhang){
            return redirect('trangcanhan');
        } else {
            return view('frontend.dangnhap');
        }
    }

    public function post_dangnhap(Request $request)
    {
        $tk = Accounts::where('tentaikhoan',$request->tentaikhoan)->where('matkhau',md5($request->matkhau))->get();
        if($tk->count() > 0){
            if($tk->first()->quyen == 2){
                session()->put('quyen','customer');
                $kh = Customers::where('id_taikhoan', $tk->first()->id_taikhoan)->first();
                session()->put('id_khachhang', $kh->id_khachhang);
                return 'customer';
            }
        } else {
            return 'err';
        }
    }

    public function dangxuat()
    {
        session()->flush();
        return redirect('/');
    }

    public function get_dangky()
    {
        return view('frontend.dangky');
    }

    public function post_dangky(Request $request)
    {
        $check = Accounts::where('tentaikhoan',$request->tentaikhoan)->count();
        if($check > 0){
            return 'err';
        } else {
            $tk = new Accounts;
            $tk->tentaikhoan = $request->tentaikhoan;
            $tk->matkhau = md5($request->matkhau);
            $tk->quyen = 2;
            $tk->save();
            $khachhang = new Customers;
            $khachhang->id_taikhoan = $tk->id_taikhoan;
            $khachhang->ten_khachhang = $request->ten_khachhang;
            $khachhang->email = $request->email;
            $khachhang->diachi = $request->diachi;
            $khachhang->dienthoai = $request->dienthoai;
            $khachhang->save();
            return 'ok';
        }
    }

    public function trangcanhan()
    {
        $id_khachhang = session()->get('id_khachhang');
        if($id_khachhang){
            $khachhang = Customers::find(session()->get('id_khachhang'));
            return view('frontend.trangcanhan',['khachhang'=>$khachhang]);
        } else {
            return redirect('dangnhap');
        }
        
    }

    public function luuttcanhan(Request $request)
    {
        $khachhang = Customers::find(session()->get('id_khachhang'));
        $khachhang->ten_khachhang = $request->ten_khachhang;
        $khachhang->diachi = $request->diachi;
        $khachhang->dienthoai = $request->dienthoai;
        $khachhang->email = $request->email;
        $khachhang->save();
        return redirect()->back()->with('succ', 'Cập nhật thành công');
    }

    public function tintuc()
    {
        $danhmuc = Categories::all();
        $tintuc = News::latest()->paginate(10);
        return view('frontend.dstintuc',['tintuc'=>$tintuc,'danhmuc'=>$danhmuc]);
    }

    public function chitiettintuc($code)
    {
        $tintuc = News::where('ma_tin', $code)->first();
        $tintucmoi = News::latest()->get()->take(10);
        return view('frontend.cttintuc',['tintuc'=>$tintuc,'tintucmoi'=>$tintucmoi]);
    }

    public function get_dathang()
    {
        $id_khachhang = session()->get('id_khachhang');
        if($id_khachhang){
            $khachhang = Customers::where('id_khachhang', $id_khachhang)->first();
            return view('frontend.dathang',['khachhang'=>$khachhang]);
        } else {
            return redirect()->back();
        }
    }

    public function dathang()
    {
        $cart = session()->get('cart');
        if($cart){
            $donhang = new Orders;
            $donhang->id_khachhang = session()->get('id_khachhang');
            $donhang->trangthai = 1;
            $donhang->save();
            foreach($cart as $id => $details){
                $sanpham = Products::where('id_sanpham', $id)->first();
                $sanpham->soluong -= $details['quantity'];
                $sanpham->save();
                $ctdonhang = new Order_detail;
                $ctdonhang->id_donhang = $donhang->id_donhang;
                $ctdonhang->id_sanpham = $id;
                $ctdonhang->soluong = $details['quantity'];
                $ctdonhang->gia = $details['price'] * $details['quantity'];
                $ctdonhang->save();
            }
            session()->flush('cart');
            return redirect('/')->with('succ','Đặt hàng thành công');
        }
    }

    public function donhang()
    {
        $donhang = Orders::where('id_khachhang', session()->get('id_khachhang'))->latest()->get();
        return view('frontend.dsdonhang',['donhang'=>$donhang]);
    }

    public function ctdonhang($id)
    {
        $donhang = Orders::where('id_donhang', $id)->first();
        $chitiet = Order_detail::where('id_donhang', $id)->leftjoin('products','order-detail.id_sanpham','=','products.id_sanpham')->select('order-detail.*','products.ten_sanpham','products.hinhanh','products.giadexuat','products.giamgia')->get();
        $tong = 0;
        foreach($chitiet as $ct){
            $tong += $ct->gia;
        }
        return view('frontend.ctdonhang',['donhang'=>$donhang,'chitietdonhang'=>$chitiet,'total'=>$tong]);
    }

    public function huydonhang($id)
    {
        $donhang = Orders::where('id_donhang', $id)->first();
        $donhang->trangthai = 3;
        $donhang->save();
        $ctdonhang = Oder_detail::where('id_donhang', $id)->get();
        foreach($ctdonhang as $ct) {
            $sanpham = Products::where('id_sanpham', $ct->id_sanpham)->first();
            $sanpham->soluong += $ct->soluong;
            $sanpham->save();
        }
        return redirect()->back()->with('succ', 'Hủy đơn hàng thành công');
    }

    public function dsphanhoi()
    {
        $id_khachhang = session()->get('id_khachhang');
        $phanhoi = Feedback_topic::where('id_khachhang', $id_khachhang)->latest()->get();
        return view('frontend.phanhoi',['phanhoi'=>$phanhoi]);
    }

    public function get_taophanhoi()
    {
        return view('frontend.phanhoimoi');
    }

    public function post_taophanhoi(Request $request)
    {
        $id_khachhang = session()->get('id_khachhang');
        $phanhoi = new Feedback_topic;
        $phanhoi->id_khachhang = $id_khachhang;
        $phanhoi->chude = $request->chude;
        $phanhoi->save();
        $ctphanhoi = new Feedback_detail;
        $ctphanhoi->id_topic = $phanhoi->id_topic;
        $ctphanhoi->id_taikhoan = Customers::find($id_khachhang)->id_taikhoan;
        $ctphanhoi->noidung = $request->noidung;
        $ctphanhoi->save();
        return redirect()->back()->with('succ', 'Tạo phản hồi thành công');
    }

    public function ctphanhoi($id)
    {
        $phanhoi = Feedback_topic::find($id);
        $ctphanhoi = Feedback_detail::where('id_topic', $id)->leftjoin('accounts','feedback-detail.id_taikhoan','=','accounts.id_taikhoan')->select('feedback-detail.*', 'accounts.tentaikhoan', 'accounts.quyen')->get();
        return view('frontend.ctphanhoi',['phanhoi'=>$phanhoi,'ctphanhoi'=>$ctphanhoi]);
    }

    public function luuphanhoi($id, Request $request)
    {
        $id_khachhang = session()->get('id_khachhang');
        $phanhoi = Feedback_topic::find($id);
        $phanhoi->touch();
        $ctphanhoi = new Feedback_detail;
        $ctphanhoi->id_topic = $id;
        $ctphanhoi->id_taikhoan = Customers::find($id_khachhang)->id_taikhoan;
        $ctphanhoi->noidung = $request->noidung;
        $ctphanhoi->save();
        return redirect()->back()->with('succ', 'Lưu phản hồi thành công');
    }

    public function timkiem(Request $request)
    {
        $danhmuc = Categories::all();
        $sanpham = Products::where('ten_sanpham', 'like', '%'.$request->timkiem.'%')->paginate(12);
        return view('frontend.kqtimkiem',['sanpham'=>$sanpham,'danhmuc'=>$danhmuc,'string'=>$request->timkiem]);
    }

    public function timkiemajax(Request $request)
    {
        $sanpham = Products::where('ten_sanpham', 'like', '%'.$request->timkiem.'%')->get()->take(10);
        $output = '<ul class="dropdown-menu" style="display:block; padding: 10px;
        width: 100%;">';
        if($sanpham->count() === 0) {
            $output .= '
               <li>Không tìm thấy sản phẩm nào</li>
               ';
        }
            foreach($sanpham as $sp)
            {
               $output .= '
               <li><a href="sanpham/'.$sp->ma_sanpham.'">'.$sp->ten_sanpham.'</a></li>
               ';
           }
           $output .= '</ul>';
           echo $output;
    }
}
