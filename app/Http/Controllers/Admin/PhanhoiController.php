<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Feedback_topic;
use App\Feedback_detail;

class PhanhoiController extends Controller
{
    public function ds()
    {
        $phanhoi = Feedback_topic::leftjoin('customers','feedback-topic.id_khachhang','=','customers.id_khachhang')->leftjoin('accounts','customers.id_taikhoan','=','accounts.id_taikhoan')->select('feedback-topic.*', 'accounts.tentaikhoan')->orderBy('updated_at','desc')->get();
        return view('admin.phanhoi.ds',['phanhoi'=>$phanhoi]);
    }

    public function chitiet($id)
    {
        $phanhoi = Feedback_topic::find($id);
        $ctphanhoi = Feedback_detail::where('id_topic', $id)->leftjoin('accounts','feedback-detail.id_taikhoan','=','accounts.id_taikhoan')->select('feedback-detail.*', 'accounts.tentaikhoan', 'accounts.quyen')->get();
        return view('admin.phanhoi.chitiet',['phanhoi'=>$phanhoi,'ctphanhoi'=>$ctphanhoi]);
    }

    public function luuphanhoi($id, Request $request)
    {
        $id_admin = session()->get('id_admin');
        $phanhoi = Feedback_topic::find($id);
        $phanhoi->touch();
        $ctphanhoi = new Feedback_detail;
        $ctphanhoi->id_topic = $id;
        $ctphanhoi->id_taikhoan = $id_admin;
        $ctphanhoi->noidung = $request->noidung;
        $ctphanhoi->save();
        return redirect()->back()->with('succ', 'Lưu phản hồi thành công');
    }
}
