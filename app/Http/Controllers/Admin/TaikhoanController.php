<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Accounts;
use App\nguoiquantri;
use App\sinhvien;
use Illuminate\Support\Str;

class TaikhoanController extends Controller
{
    public function get_dangnhap()
    {
        if(session()->get('quyen') == "admin"){
            return redirect(route('thongke'));
        } else {
            return view('admin.dangnhap');
        }
    }

    public function post_dangnhap(Request $request)
    {
        $tk = Accounts::where('tentaikhoan',$request->tentaikhoan)->where('matkhau',md5($request->matkhau))->get();
        if($tk->count() > 0){
            if($tk->first()->quyen == 1){
                session()->put('quyen','admin');
                session()->put('id_admin',$tk->first()->id_taikhoan);
                return 'admin';
            }
        } else {
            return 'err';
        }
    }

    public function dangxuat(){
        session()->flush();
        return redirect('/admin');
    }

    public function ds()
    {
        $tk = Accounts::latest()->get();
        return view('admin.taikhoan.ds',['taikhoan'=>$tk]);
    }

    public function post_them(Request $request)
    {
        $check = Accounts::where('tentaikhoan', $request->tentk)->count();
        if($check > 1){
            return 'err';
        } else {
            $tk = new Accounts;
            $tk->tentaikhoan = $request->tentk;
            $tk->matkhau = md5($request->matkhau);
            $tk->quyen = 1;
            $tk->save();
            return 'ok'; 
        }
    }

    public function xoa($id)
    {
        $tk = Accounts::find($id)->delete();
        return back()->with('succ','Xóa thành công');
    }
    
    public function init_admin()
    {
        $tk = new Accounts;
        $tk->tentaikhoan = 'admin';
        $tk->matkhau = md5('admin');
        $tk->quyen = 1;
        $tk->save();
        return "Tạo admin thành công";
    }
}
