<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;

class TintucController extends Controller
{
    public function ds()
    {
        $tt = News::all();
        return view('admin.tintuc.ds',['tintuc'=>$tt]);
    }

    public function get_them()
    {
        return view('admin.tintuc.them');
    }

    public function post_them(Request $request)
    {
        $tt = new News;
        $tt->ten_tintuc = $request->ten_tintuc;
        $tt->ma_tin = $request->ma_tin;
        if($request->hasFile('hinhanh')){
            $file = $request->file('hinhanh');
            $name = $file->getClientOriginalName(); //Lấy tên file
            $destination = base_path() . '/public/assets/images/tintuc';
            $file->move($destination, $name);
            $tt->hinhanh = $name;
        }
        $tt->noidung = $request->noidung;
        $tt->tinhtrang = $request->tinhtrang;
        $tt->save();
        return redirect()->route('dstt')->with('succ','Thêm thành công');
    }

    public function get_sua($id)
    {
        $tt = News::find($id);
        return view('admin.tintuc.sua',['tintuc'=>$tt]);
    }

    public function post_sua($id, Request $request)
    {
        $tt = News::find($id);
        $tt->ten_tintuc = $request->ten_tintuc;
        $tt->ma_tin = $request->ma_tin;
        if($request->hasFile('hinhanh')){
            $file = $request->file('hinhanh');
            $name = $file->getClientOriginalName(); //Lấy tên file
            $destination = base_path() . '/public/assets/images/tintuc';
            $file->move($destination, $name);
            $tt->hinhanh = $name;
        }
        $tt->noidung = $request->noidung;
        $tt->tinhtrang = $request->tinhtrang;
        $tt->save();
        return back()->with('succ','Sửa thành công');
    }
    
    public function xoa($id)
    {
        $tt = News::find($id)->delete();
        return back()->with('succ','Xóa thành công');
    }
}
