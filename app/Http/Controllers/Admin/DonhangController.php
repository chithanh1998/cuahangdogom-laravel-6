<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Orders;
use App\Customers;
use App\Order_detail;

class DonhangController extends Controller
{
    public function ds()
    {
        $donhang = Orders::leftjoin('customers','orders.id_khachhang','=','customers.id_khachhang')->select('orders.*', 'customers.ten_khachhang')->latest()->get();
        return view('admin.donhang.ds',['donhang'=>$donhang]);
    }

    public function chitiet($id)
    {
        $donhang = Orders::where('id_donhang', $id)->leftjoin('customers','orders.id_khachhang','=','customers.id_khachhang')->select('orders.*','customers.ten_khachhang','customers.diachi','customers.dienthoai')->get()->first();
        $chitiet = Order_detail::where('id_donhang', $id)->leftjoin('products','order-detail.id_sanpham','=','products.id_sanpham')->select('order-detail.*','products.ten_sanpham','products.hinhanh','products.giadexuat','products.giamgia')->get();
        $tong = 0;
        foreach($chitiet as $ct){
            $tong += $ct->gia;
        }
        return view('admin.donhang.chitiet',['donhang'=>$donhang,'chitiet'=>$chitiet,'tongcong'=>$tong]);
    }

    public function chuyentrangthai($id)
    {
        $donhang = Orders::where('id_donhang', $id)->first();
        if($donhang->trangthai !== 2 && $donhang->trangthai !== 3){
            $donhang->trangthai += 1;
            $donhang->save();
            return redirect()->back()->with('succ', 'Cập nhật trạng thái thành công');
        }
    }
    
    public function huy($id)
    {
        $donhang = Orders::where('id_donhang', $id)->first();
        if($donhang->trangthai !== 2){
            $donhang->trangthai = 3;
            $donhang->save();
            return redirect()->back()->with('succ', 'Cập nhật trạng thái thành công');
        }
    }
}
