<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customers;
use App\Accounts;
use App\Orders;

class KhachhangController extends Controller
{
    public function ds()
    {
        $khachhang = Customers::leftjoin('accounts','customers.id_taikhoan','=','accounts.id_taikhoan')->select('customers.*','accounts.tentaikhoan')->latest()->get();
        return view('admin.khachhang.ds',['khachhang'=>$khachhang]);
    }

    public function post_them(Request $request)
    {
        $check = Accounts::where('tentaikhoan',$request->tentk)->count();
        if($check > 0){
            return 'err';
        } else {
            $tk = new Accounts;
            $tk->tentaikhoan = $request->tentk;
            $tk->matkhau = md5($request->matkhau);
            $tk->quyen = 2;
            $tk->save();
            $khachhang = new Customers;
            $khachhang->id_taikhoan = $tk->id_taikhoan;
            $khachhang->ten_khachhang = $request->ten_khachhang;
            $khachhang->email = $request->email;
            $khachhang->diachi = $request->diachi;
            $khachhang->dienthoai = $request->dienthoai;
            $khachhang->save();
            return 'ok';
        }
    }

    public function thongtin(Request $request)
    {
        $khachhang = Customers::find($request->id);
        return json_encode($khachhang);
    }

    public function post_sua(Request $request)
    {
        $khachhang = Customers::find($request->id);
        $khachhang->ten_khachhang = $request->ten_khachhang;
            $khachhang->email = $request->email;
            $khachhang->diachi = $request->diachi;
            $khachhang->dienthoai = $request->dienthoai;
            $khachhang->save();
        return 'ok';
    }
    
    public function xoa($id)
    {
        $khachhang = Customers::find($id);
        $check = Orders::where('id_khachhang',$khachhang->id_khachhang)->count();
        if($check > 1){
            return back()->with('err','Không thể xóa, hãy xóa đơn hàng trước');
        } else {
            $khachhang->delete();
            return back()->with('succ','Xóa thành công');
        }
    }
}
