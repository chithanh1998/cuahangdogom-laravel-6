<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Settings;
use App\Banner;

class CaidatController extends Controller
{
    public function caidat()
    {
        $banner = Banner::all();
        $caidat = Settings::first();
        return view('admin.caidat.caidat',['caidat'=>$caidat,'banner'=>$banner]);
    }

    public function luucaidat(Request $request)
    {
        $caidat = Settings::find(1);
        if(empty($caidat)) {
            $caidat = new Settings;
        }
        $caidat->tencuahang = $request->tencuahang;
        $caidat->diachi = $request->diachi;
        $caidat->dienthoai = $request->dienthoai;
        $caidat->email = $request->email;
        $caidat->facebook = $request->facebook;
        if($request->hasFile('logo')){
            $file = $request->file('logo');
            $name = $file->getClientOriginalName(); //Lấy tên file
            $destination = base_path() . '/public/assets/images/logo';
            $file->move($destination, $name);
            $caidat->logo = $name;
        }
        if($request->hasFile('banner')){
            $file = $request->file('banner');
            $name = $file->getClientOriginalName(); //Lấy tên file
            $destination = base_path() . '/public/assets/images/banner';
            $file->move($destination, $name);
            $banner = new Banner;
            $banner->tieude = 'tieude';
            $banner->hinhanh = $name;
            $banner->trangthai = 1;
            $banner->save();
        }
        $caidat->save();
        return back()->with('succ','Lưu thành công');
    }
    
    public function xoabanner($id)
    {
        $banner = Banner::find($id)->delete();
        return back()->with('succ','Xóa thành công');
    }
}
