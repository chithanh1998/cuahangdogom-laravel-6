<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customers;
use App\Products;
use App\Orders;
use App\Order_detail;
use Carbon\Carbon;

class ThongkeController extends Controller
{
    public function thongke()
    {
        $chart = [];
        $data = [];
        $data['khachhangmoi'] = Customers::whereDate('created_at','=', Carbon::now()->toDateString())->count();
        $data['khachhangmoi-hq'] = Customers::whereDate('created_at','=', Carbon::yesterday()->toDateString())->count();
        $data['donhangmoi'] = Orders::whereDate('created_at','=', Carbon::now()->toDateString())->count();
        $data['donhangmoi-hq'] = Orders::whereDate('created_at','=', Carbon::yesterday()->toDateString())->count();
        $chitietdonhang = Order_detail::leftjoin('orders','order-detail.id_donhang','=','orders.id_donhang')->select('order-detail.*','orders.trangthai')->where('orders.trangthai', 2)->whereDate('orders.updated_at','=', Carbon::now()->toDateString())->get();
        $doanhthu = 0;
        foreach($chitietdonhang as $dh){
            $doanhthu += $dh->gia;
        }
        $data['doanhthu'] = $doanhthu;
        $chitietdonhang_hq = Order_detail::leftjoin('orders','order-detail.id_donhang','=','orders.id_donhang')->select('order-detail.*','orders.trangthai','orders.updated_at as capnhat')->where('orders.trangthai', 2)->whereDate('orders.updated_at','=', Carbon::yesterday()->toDateString())->get();
        $doanhthu_hq = 0;
        foreach($chitietdonhang_hq as $dh){
            $doanhthu_hq += $dh->gia;
        }
        $data['doanhthu-hq'] = $doanhthu_hq;
        for($i=7; $i >= 0; $i--){
            $day = Carbon::now()->subDays($i)->toDateString();
            $soluong = 0;
            $donhang = Order_detail::leftjoin('orders','order-detail.id_donhang','=','orders.id_donhang')->select('order-detail.*','orders.trangthai','orders.updated_at as capnhat')->where('orders.trangthai', 2)->whereDate('orders.updated_at','=', $day)->get();
            foreach($donhang as $dh){
                $soluong += $dh->soluong;
            }
            $chart['data'][] = $soluong;
            $chart['label'][] = $day;
        }
        $banchay = Order_detail::leftjoin('orders','order-detail.id_donhang','=','orders.id_donhang')->where('orders.trangthai', 2)->groupBy('id_sanpham')->selectRaw('sum(soluong) as sum, id_sanpham')->orderBy('sum', 'desc')->get()->take(5);
        $sanpham = Products::all();
        return view('admin.thongke.thongke',['data'=>$data, 'chart'=>$chart, 'banchay'=>$banchay,'sanpham'=>$sanpham]);
    }
}
