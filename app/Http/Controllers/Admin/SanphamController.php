<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products;
use App\Categories;
use App\Order_detail;

class SanphamController extends Controller
{
    public function ds()
    {
        $sanpham = Products::leftjoin('categories','products.id_loaisp','=','categories.id_loaisanpham')->select('products.*','categories.ten_loaisanpham as loaisanpham')->get();
        return view('admin.sanpham.ds',['sanpham'=>$sanpham]);
    }
    
    public function get_sua($id)
    {
        $sanpham = Products::find($id);
        $loaisanpham = Categories::all();
        return view('admin.sanpham.sua',['sanpham'=>$sanpham, 'loaisanpham'=>$loaisanpham]);
    }

    public function get_them()
    {
        $loaisanpham = Categories::all();
        return view('admin.sanpham.them',['loaisanpham'=>$loaisanpham]);
    }

    public function post_them(Request $request)
    {
        $check = Products::where('ma_sanpham',$request->ma_sanpham)->count();
        if($check > 0){
            return back()->with('err','Mã sản phẩm đã tồn tại');
        } else {
            $sanpham = new Products;
            $sanpham->ten_sanpham = $request->ten_sanpham;
            $sanpham->ma_sanpham = $request->ma_sanpham;
            $sanpham->giadexuat = $request->giadexuat;
            $sanpham->giamgia = $request->giamgia;
            $sanpham->soluong = $request->soluong;
            $sanpham->id_loaisp = $request->id_loaisp;
            $sanpham->tinhtrang = $request->tinhtrang;
            $sanpham->noidung = $request->noidung;
            if($request->hasFile('hinhanh')){
                $file = $request->file('hinhanh');
                $name = $file->getClientOriginalName(); //Lấy tên file
                $destination = base_path() . '/public/assets/images/sanpham';
                $file->move($destination, $name);
                $sanpham->hinhanh = $name;
            }
            $sanpham->save();
            return back()->with('succ','Thêm thành công');
        }
    }

    public function post_sua(Request $request)
    {
        $check = Products::where('ma_sanpham',$request->ma_sanpham)->where('id_sanpham', '!=', $request->id_sanpham)->count();
        if($check > 0){
            return back()->with('err','Trùng mã sản phẩm');
        } else {
            $sanpham = Products::find($request->id_sanpham);
            $sanpham->ten_sanpham = $request->ten_sanpham;
            $sanpham->ma_sanpham = $request->ma_sanpham;
            if($request->hasFile('hinhanh')){
                $file = $request->file('hinhanh');
                $name = $file->getClientOriginalName(); //Lấy tên file
                $destination = base_path() . '/public/assets/images/sanpham';
                $file->move($destination, $name);
                $sanpham->hinhanh = $name;
            }
            $sanpham->giadexuat = $request->giadexuat;
            $sanpham->giamgia = $request->giamgia;
            if($request->tinhtrang == 0){
                $sanpham->soluong = 0;
            } else {
                $sanpham->soluong = $request->soluong;
            }
            $sanpham->id_loaisp = $request->id_loaisp;
            if($request->soluong == 0){
                $sanpham->tinhtrang = 0;
            } else {
                $sanpham->tinhtrang = $request->tinhtrang;
            }
            $sanpham->noidung = $request->noidung;
            $sanpham->save();
            return back()->with('succ','Sửa thành công');
        }
    }

    public function xoa($id)
    {
        $sanpham = Products::find($id);
        $check = Order_detail::where('id_sanpham',$sanpham->id_sanpham)->count();
        if($check > 1){
            return back()->with('err','Không thể xóa, hãy xóa đơn hàng trước');
        }
            $sanpham->delete();
            return back()->with('succ','Xóa thành công');
    }

    //
    // Loại sản phẩm
    //------------------------------------------------
    public function dsloai()
    {
        $loaisp = Categories::all();
        return view('admin.sanpham.loaisp',['loaisanpham'=>$loaisp]);
    }

    public function post_themloai(Request $request)
    {
        $check = Categories::where('ten_loaisanpham',$request->ten_loaisanpham)->count();
        if($check > 0){
            return 'err';
        } else {
            $loaisanpham = new Categories;
            $loaisanpham->ten_loaisanpham = $request->ten_loaisanpham;
            $loaisanpham->save();
            return 'ok';
        }
    }

    public function get_thongtinloaisp(Request $request)
    {
        $loaisanpham = Categories::find($request->id);
        return json_encode($loaisanpham);
    }

    public function post_sualoai(Request $request)
    {
        $check = Categories::where('ten_loaisanpham',$request->ten_loaisanpham)->count();
        if($check > 0){
            return 'err';
        } else {
            $loaisanpham = Categories::find($request->id_loaisanpham);
            $loaisanpham->ten_loaisanpham = $request->ten_loaisanpham;
            $loaisanpham->save();
            return 'ok';
        }
    }

    public function xoaloai($id)
    {
        $loaisanpham = Categories::find($id);
        $check = Products::where('id_loaisp',$loaisanpham->id_loaisanpham)->count();
        if($check > 1){
            return back()->with('err','Không thể xóa, đang có sản phẩm có loại sản phẩm này');
        }
            $loaisanpham->delete();
            return back()->with('succ','Xóa thành công');
    }
}
