<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\Order_detail;
use App\Settings;
use App\Banner;
use View;

class CartController extends Controller
{
    public function __construct() {
        $caidat = Settings::first();
        $banner = Banner::all();
        View::share ( 'caidat', $caidat );
        View::share ( 'banner', $banner );
    }

    public function cart()
    {
        $sanphammoi = Products::latest()->get()->take(5);
        $banchay = Order_detail::leftjoin('orders','order-detail.id_donhang','=','orders.id_donhang')->where('orders.trangthai', 4)->groupBy('id_sanpham')->selectRaw('sum(soluong) as sum, id_sanpham')->orderBy('sum', 'desc')->get()->take(5);
        $sanpham = Products::all();
        return view('frontend.giohang',['sanphammoi'=>$sanphammoi,'banchay'=>$banchay,'sanpham'=>$sanpham]);
    }

    public function addToCart($id, Request $request)
    {
        $product = Products::find($id);

        if(!$product) {

            abort(404);

        }

        if($request->qty > $product->soluong)
        {
            return redirect()->back()->with('err', 'Số lượng nhập vào không đúng');
        }

        $cart = session()->get('cart');

        // if cart is empty then this the first product
        if(!$cart) {

            $cart = [
                    $id => [
                        "name" => $product->ten_sanpham,
                        "quantity" => $request->qty,
                        "price" => $product->giadexuat - $product->giamgia,
                        "photo" => $product->hinhanh
                    ]
            ];

            session()->put('cart', $cart);

            return redirect()->back()->with('succ', 'Thêm sản phẩm thành công');
        }

        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$id])) {

            $cart[$id]['quantity']++;

            session()->put('cart', $cart);

            return redirect()->back()->with('succ', 'Thêm sản phẩm thành công');

        }

        // if item not exist in cart then add to cart with quantity
        $cart[$id] = [
            "name" => $product->ten_sanpham,
            "quantity" => $request->qty,
            "price" => $product->giadexuat - $product->giamgia,
            "photo" => $product->hinhanh
        ];

        session()->put('cart', $cart);

        return redirect()->back()->with('succ', 'Thêm sản phẩm thành công');
    }
    
    public function update(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');

            $cart[$request->id]["quantity"] = $request->quantity;

            session()->put('cart', $cart);

            session()->flash('succ', 'Giỏ hàng cập nhật thành công');
        }
    }

    public function remove(Request $request)
    {
        if($request->id) {

            $cart = session()->get('cart');

            if(isset($cart[$request->id])) {

                unset($cart[$request->id]);

                session()->put('cart', $cart);
            }

            session()->flash('succ', 'Xóa sản phẩm thành công');
        }
    }
}
