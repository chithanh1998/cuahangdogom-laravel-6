<?php

namespace App\Http\Middleware;

use Closure;

class customer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('id_khachhang')){
            return $next($request);
        } else {
        return redirect('dangnhap')->with('err','Đăng nhập để tiếp tục');
        }
    }
}
