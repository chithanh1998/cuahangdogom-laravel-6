@extends('admin.layout.main')
@section('css')
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Chi tiết đơn hàng </h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <!-- <div class="panel-heading">
                                        <h4>Invoice</h4>
                                    </div> -->
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="pull-left">
                                                <h3>Chi tiết đơn hàng</h3>
                                            </div>
                                            <div class="pull-right">
                                                <h4>Đơn hàng # {{$donhang->id_donhang}}<br>
                                                    
                                                </h4>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="pull-left m-t-30">
                                                    <address>
                                                      <strong>{{$donhang->ten_khachhang}}</strong><br>
                                                      {{$donhang->diachi}}<br>
                                                      {{$donhang->dienthoai}}
                                                      </address>
                                                </div>
                                                <div class="pull-right m-t-30">
                                                    <p><strong>Ngày đặt: </strong> @datetime($donhang->created_at)</p>
                                                    <p><strong>Trạng thái: </strong>@if($donhang->trangthai === 1) <span class="label label-danger">Chưa giao</span>@elseif($donhang->trangthai === 2)<span class="label label-success">Đã giao</span>@else<span class="label label-secondary">Đã hủy</span>@endif</p>
                                                    <p><strong>Mã đơn: </strong> #{{$donhang->id_donhang}}</p>
                                                </div>
                                            </div><!-- end col -->
                                        </div>
                                        <!-- end row -->

                                        <div class="m-h-50"></div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table m-t-30">
                                                        <thead>
                                                            <tr><th>#</th>
                                                            <th>Hình ảnh</th>
                                                            <th>Sản phẩm</th>
                                                            <th>Số lượng</th>
                                                            <th>Giá</th>
                                                            <th>Tổng</th>
                                                        </tr></thead>
                                                        <tbody>
                                                            @foreach($chitiet as $ct)
                                                            <tr>
                                                                <td>{{$loop->iteration}}</td>
                                                                <td><img src="assets/images/sanpham/{{$ct->hinhanh}}" alt="" width="100"></td>
                                                                <td>{{$ct->ten_sanpham}}</td>
                                                                <td>{{$ct->soluong}}</td>
                                                                <td>@money($ct->giadexuat)</td>
                                                                <td>@money($ct->gia)</td>
                                                            </tr>
                                                            @endforeach
                                                
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-6 col-md-offset-3">
                                                <h3 class="text-right">@money($tongcong)</h3>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="hidden-print">
                                            <div class="pull-right">
                                            @if($donhang->trangthai === 1)
                                                <a href="admin/donhang/chitiet/{{$donhang->id_donhang}}/huy" class="btn btn-inverse waves-effect waves-light">Hủy</a>
                                                <a href="admin/donhang/chitiet/{{$donhang->id_donhang}}/chuyentrangthai" class="btn btn-primary waves-effect waves-light">
                                                Đã giao
                                                </a>
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>



@endsection
@section('js')
<script src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
@endsection
@section('script')
<script>
    @if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
    function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};
function confirmationDelete(anchor) {
    swal({
                title: "Bạn chắc chắn muốn xóa?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Có",
                cancelButtonText: "Không",
                closeOnConfirm: false
            }, function () {
                window.location = anchor.attr("href"); 
            });
}
$(document).ready(function () {
    $('#datatable').dataTable();

            });
        </script>
@endsection