@extends('admin.layout.main')
@section('css')
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Chỉnh sửa sản phẩm</h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 header-title"><b>{{$sanpham->ten_sanpham}}</b></h4>
                        			<div class="row">
                        				<div class="col-md-12">
                        					<form class="form-horizontal" method="POST" action="{{route('luuchinhsuasp')}}" enctype="multipart/form-data">
											@csrf
                                            <input type="hidden" name="id_sanpham" value="{{$sanpham->id_sanpham}}">
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">Tên sản phẩm</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" name="ten_sanpham" value="{{$sanpham->ten_sanpham}}">
	                                                </div>
	                                            </div>
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label" for="ma_sanpham">Mã sản phẩm</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" id="ma_sanpham" name="ma_sanpham" class="form-control" value="{{$sanpham->ma_sanpham}}">
	                                                </div>
	                                            </div>
                                                <div class="form-group">
	                                                <label class="col-sm-2 control-label">Loại sản phẩm</label>
	                                                <div class="col-sm-10">
	                                                    <select class="form-control" name="id_loaisp">
                                                        @foreach($loaisanpham as $lsp)
	                                                        <option value="{{$lsp->id_loaisanpham}}" @if($sanpham->id_loaisanpham === $lsp->id_loaisanpham) selected @endif>{{$lsp->ten_loaisanpham}}</option>
	                                                    @endforeach
	                                                        
	                                                    </select>
	                                                </div>
	                                            </div>
	                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Hình ảnh</label>
	                                                <div class="col-md-10">
                                                    <input type="file" name="hinhanh" /><img src="assets/images/sanpham/{{$sanpham->hinhanh}}" alt="" width="500">
	                                                </div>
                                                </div>

                                                <div class="form-group">
	                                                <label class="col-md-2 control-label" for="giadexuat">Giá đề xuất</label>
	                                                <div class="col-md-10">
	                                                    <input type="number" id="giadexuat" name="giadexuat" class="form-control" value="{{$sanpham->giadexuat}}">
	                                                </div>
	                                            </div>

                                                <div class="form-group">
	                                                <label class="col-md-2 control-label" for="giamgia">Giảm giá</label>
	                                                <div class="col-md-10">
	                                                    <input type="number" id="giamgia" name="giamgia" class="form-control" value="{{$sanpham->giamgia}}">
	                                                </div>
	                                            </div>

                                                <div class="form-group">
	                                                <label class="col-md-2 control-label" for="giadexuat">Số lượng</label>
	                                                <div class="col-md-10">
	                                                    <input type="number" id="giadexuat" name="soluong" requỉed class="form-control" value="{{$sanpham->soluong}}">
	                                                </div>
	                                            </div>

                                                <div class="form-group">
	                                                <label class="col-sm-2 control-label">Tình trạng</label>
	                                                <div class="col-sm-10">
	                                                    <select class="form-control" name="tinhtrang">
	                                                        <option value="1" @if($sanpham->tinhtrang === 1) selected @endif>Trong kho</option>
	                                                        <option value="0" @if($sanpham->tinhtrang === 0) selected @endif>Hết hàng</option>
	                                                        
	                                                    </select>
	                                                </div>
	                                            </div>

	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">Nội dung</label>
	                                                <div class="col-md-10">
	                                                    <textarea class="form-control" rows="5" name="noidung">{!! $sanpham->noidung !!}</textarea>
	                                                </div>
	                                            </div>

                                                <button type="submit" class="btn btn-info waves-effect waves-light">Lưu</button>

	                                        </form>
                        				</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>



@endsection
@section('js')
<script src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
@endsection
@section('script')
<script>
    @if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
    function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};
function confirmationDelete(anchor) {
    swal({
                title: "Bạn chắc chắn muốn xóa?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Có",
                cancelButtonText: "Không",
                closeOnConfirm: false
            }, function () {
                window.location = anchor.attr("href"); 
            });
}
$(document).ready(function () {
    $('#datatable').dataTable();

            });
        </script>
        @include('ckfinder::setup')
        <script src={{ url('ckeditor/ckeditor.js') }}></script>
    <script>
    CKEDITOR.replace( 'noidung', {
        filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',

    } );
    </script>
    
@endsection