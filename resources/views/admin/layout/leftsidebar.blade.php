<div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <ul>
                            <li>
                                <a href="{{route('thongke')}}" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span> Thống kê </span></a>
                            </li>
                            <li>
                                <a href="{{route('dsqt')}}" class="waves-effect"><i class="mdi mdi-account"></i><span> Tài khoản </span></a>
                            </li>
                            <li>
                                <a href="{{route('dskhachhang')}}" class="waves-effect"><i class="mdi mdi-account-multiple"></i><span> Khách hàng </span></a>
                            </li>
                            <li>
                                <a href="{{route('dssp')}}" class="waves-effect"><i class="mdi mdi-basket"></i><span> Sản phẩm </span></a>
                            </li>
                            <li>
                                <a href="{{route('dsloaisp')}}" class="waves-effect"><i class="mdi mdi-apps"></i><span>Loại sản phẩm </span></a>
                            </li>
                            <li>
                                <a href="{{route('dsdonhang')}}" class="waves-effect"><i class="mdi mdi-view-list"></i><span> Đơn hàng </span></a>
                            </li>
                            <li>
                                <a href="{{route('dsphanhoi')}}" class="waves-effect"><i class="mdi mdi-contact-mail"></i><span> Phản hồi </span></a>
                            </li>
                            <li>
                                <a href="{{route('dstt')}}" class="waves-effect"><i class="mdi mdi-newspaper"></i><span> Tin tức </span></a>
                            </li>
                            <li>
                                <a href="{{route('dscaidat')}}" class="waves-effect"><i class="mdi mdi-settings"></i><span> Cài đặt </span></a>
                            </li>
                        

                        </ul>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>