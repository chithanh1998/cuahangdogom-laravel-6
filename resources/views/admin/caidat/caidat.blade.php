@extends('admin.layout.main')
@section('css')
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Cài đặt</h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 header-title"><b>Cài đặt</b></h4>
                        			<div class="row">
                        				<div class="col-md-12">
                        					<form class="form-horizontal" method="POST" action="{{route('luuchinhsuacaidat')}}" enctype="multipart/form-data">
											@csrf
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">Tên cửa hàng</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" name="tencuahang" value="@if(isset($caidat->tencuahang)){{$caidat->tencuahang}}@endif">
	                                                </div>
	                                            </div>
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label" for="diachi">Địa chỉ</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" id="diachi" name="diachi" class="form-control" value="@if(isset($caidat->diachi)){{$caidat->diachi}}@endif">
	                                                </div>
	                                            </div>
                                                <div class="form-group">
	                                                <label class="col-md-2 control-label" for="dienthoai">Điện thoại</label>
	                                                <div class="col-md-10">
	                                                    <input type="number" id="dienthoai" name="dienthoai" class="form-control" value="@if(isset($caidat->dienthoai)){{$caidat->dienthoai}}@endif" />
	                                                </div>
	                                            </div>
                                                <div class="form-group">
	                                                <label class="col-md-2 control-label" for="email">Email</label>
	                                                <div class="col-md-10">
	                                                    <input type="email" id="email" name="email" class="form-control" value="@if(isset($caidat->email)){{$caidat->email}}@endif">
	                                                </div>
	                                            </div>
                                                <div class="form-group">
	                                                <label class="col-md-2 control-label" for="facebook">Facebook</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" id="facebook" name="facebook" class="form-control" value="@if(isset($caidat->facebook)){{$caidat->facebook}}@endif">
	                                                </div>
	                                            </div>
	                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Logo</label>
	                                                <div class="col-md-10">
                                                    <input type="file" name="logo" />
                                                    <img src="assets/images/logo/@if(isset($caidat->logo)){{$caidat->logo}}@endif" alt="" width="300">
	                                                </div>
                                                </div>

                                                <label class="col-md-2 control-label">Thêm banner</label>
	                                                <div class="col-md-10">
                                                    <input type="file"  name="banner" />
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Hình ảnh</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($banner as $bn)
                                                            <tr>
                                                                <td scope="row">{{ $loop->iteration }}</td>
                                                                <td><img src="assets/images/banner/{{$bn->hinhanh}}" alt="" width="200"></td>
                                                                <td><a href="admin/banner/xoa/{{$bn->id}}" onclick="javascript:confirmationDelete($(this));return false;"><i class="fa fa-trash"></i></a></td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    
	                                                </div>
                                                </div>

                                                <button type="submit" class="btn btn-info waves-effect waves-light">Lưu</button>

	                                        </form>
                        				</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>



@endsection
@section('js')
<script src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
@endsection
@section('script')
<script>
    @if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
    function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};
function confirmationDelete(anchor) {
    swal({
                title: "Bạn chắc chắn muốn xóa?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Có",
                cancelButtonText: "Không",
                closeOnConfirm: false
            }, function () {
                window.location = anchor.attr("href"); 
            });
}
$(document).ready(function () {
    $('#datatable').dataTable();

            });
        </script>
        @include('ckfinder::setup')
        <script src={{ url('ckeditor/ckeditor.js') }}></script>
    <script>
    CKEDITOR.replace( 'noidung', {
        filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',

    } );
    </script>
    
@endsection