@extends('admin.layout.main')
@section('css')

@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Thống kê </h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">

                            <div class="col-lg-4 col-md-12">
                                <div class="card-box widget-box-two widget-two-success">
                                    <i class="mdi mdi-account-convert widget-two-icon"></i>
                                    <div class="wigdet-two-content">
                                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Số khách hàng">Khách hàng mới</p>
                                        <h2><span data-plugin="counterup">{{$data['khachhangmoi']}}</span>@if($data['khachhangmoi'] > $data['khachhangmoi-hq']) <small><i class="mdi mdi-arrow-up text-success"></i></small>@endif</h2>
                                        <p class="text-muted m-0"><b>Hôm qua:</b> {{$data['khachhangmoi-hq']}}</p>
                                    </div>
                                </div>
                            </div><!-- end col -->

                            <div class="col-lg-4 col-md-12">
                                <div class="card-box widget-box-two widget-two-warning">
                                    <i class="mdi mdi-layers widget-two-icon"></i>
                                    <div class="wigdet-two-content">
                                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Số đơn hàng">Đơn hàng mới</p>
                                        <h2><span data-plugin="counterup">{{$data['donhangmoi']}} </span> @if($data['donhangmoi'] > $data['donhangmoi-hq']) <small><i class="mdi mdi-arrow-up text-success"></i></small>@endif</h2>
                                        <p class="text-muted m-0"><b>Hôm qua:</b> {{$data['donhangmoi-hq']}}</p>
                                    </div>
                                </div>
                            </div><!-- end col -->

                            <div class="col-lg-4 col-md-12">
                                <div class="card-box widget-box-two widget-two-primary">
                                    <i class="mdi mdi-chart-areaspline widget-two-icon"></i>
                                    <div class="wigdet-two-content">
                                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Doanh thu">Doanh thu</p>
                                        <h2><span data-plugin="counterup">{{$data['doanhthu']}} </span> @if($data['doanhthu'] > $data['doanhthu-hq']) <small><i class="mdi mdi-arrow-up text-success"></i></small>@endif</h2>
                                        <p class="text-muted m-0"><b>Hôm qua:</b> @money($data['doanhthu-hq'])</p>
                                    </div>
                                </div>
                            </div><!-- end col -->


                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Tổng sản phẩm bán được</h4>

                                    <canvas id="Chart1" style="height: 320px;" class="flot-chart"></canvas>
                                </div>
                            </div>




                            <div class="col-md-6">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Sản phẩm bán chạy</h4>

                                    <div class="table-responsive">
                                        <table class="table table table-hover m-0">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Tên</th>
                                                    <th>Số lượng</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                @foreach($banchay as $bc)
                                                @foreach($sanpham as $sp)
                                                    @if($bc->id_sanpham === $sp->id_sanpham)
                                                <tr>
                                                    <th>
                                                        <img src="assets/images/sanpham/{{$sp->hinhanh}}" alt="user" class="thumb-sm img-circle" width="25"/>
                                                    </th>
                                                    <td>
                                                    
                                                        <h5 class="m-0">{{$sp->ten_sanpham}}</h5>
                                                    
                                                    </td>
                                                    <td>{{$bc->sum}}</td>
                                                </tr>
                                                @endif
                                                    @endforeach
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div> <!-- table-responsive -->
                                </div> <!-- end card -->
                            </div>
                            <!-- end col -->
                            </div>
                        <!-- end row -->
                    </div> <!-- container -->

                </div> <!-- content -->

            </div>
@endsection
@section('js')
<!-- Counter js  -->
        <script src="plugins/waypoints/jquery.waypoints.min.js"></script>
        <script src="plugins/counterup/jquery.counterup.min.js"></script>

        <!-- Chart JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.0.2/chart.js" integrity="sha512-n8DscwKN6+Yjr7rI6mL+m9nS4uCEgIrKRFcP0EOkIvzOLUyQgOjWK15hRfoCJQZe0s6XrARyXjpvGFo1w9N3xg==" crossorigin="anonymous"></script>
        <script src="plugins/moment/moment.js"></script>
        <script src="plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
@endsection
@section('script')
<script>
$(document).ready(function(){
var ctx1 = document.getElementById('Chart1');
var Chart1 = new Chart(ctx1, {
    type: 'line',
    data: {
        labels: [@if(isset($chart["label"]))@foreach($chart["label"] as $lb)'{{$lb}}', @endforeach @endif],
        datasets: [{
            label: 'Sản phẩm',
            data: [@if(isset($chart["data"]))@foreach($chart["data"] as $dt){{$dt}}, @endforeach @endif],
			backgroundColor: 'rgba(255, 184, 34, 1)',
			borderColor: 'rgba(255, 184, 34, 1)',
			fill: false,
		},
	]
    },
    options: {
		responsive: true,
		tooltips: {
			mode: 'nearest',
			intersect: false,
		},
        scales: {
          yAxes: [{
            ticks: {
                beginAtZero: true,
                callback: function (value) { if (Number.isInteger(value)) { return value; } },
                stepSize: 1
            }
          }]
        }
		
    }
});
})
</script>
@endsection