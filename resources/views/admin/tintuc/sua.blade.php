@extends('admin.layout.main')
@section('css')
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<!-- Sweet Alert -->
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Tin tức </h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                <div class="row">
                                    <h4 class="m-t-5 header-title"><b>Sửa tin tức</b></h4>
                                    </div>
                                    <div class="col-md-12">
                        					<form class="form-horizontal" method="POST" action="admin/tintuc/sua/{{$tintuc->id_tintuc}}" enctype="multipart/form-data">
											@csrf
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">Tên tin tức</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" value="{{$tintuc->ten_tintuc}}" name="ten_tintuc" required>
	                                                </div>
	                                            </div>
												<div class="form-group">
	                                                <label class="col-md-2 control-label">Mã tin tức</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" value="{{$tintuc->ma_tin}}" name="ma_tin" required>
	                                                </div>
	                                            </div>
												<div class="form-group">
                                                <label class="col-md-2 control-label">Hình ảnh</label>
	                                                <div class="col-md-10">
                                                    <input type="file" name="hinhanh" />
													<img src="assets/images/tintuc/@if(isset($tintuc->hinhanh)){{$tintuc->hinhanh}}@endif" alt="" width="200">
	                                                </div>
                                                </div>
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">Nội dung</label>
	                                                <div class="col-md-10">
	                                                    <textarea class="form-control" id="noidung" name="noidung">{!!$tintuc->noidung!!}</textarea>
	                                                </div>
	                                            </div>

												<div class="form-group">
	                                                <label class="col-sm-2 control-label">Tình trạng</label>
	                                                <div class="col-sm-10">
	                                                    <select class="form-control" name="tinhtrang">
	                                                        <option value="1" @if($tintuc->tinhtrang === 1) selected @endif>Hiện</option>
	                                                        <option value="0" @if($tintuc->tinhtrang === 0) selected @endif>Ẩn</option>
	                                                        
	                                                    </select>
	                                                </div>
	                                            </div>
                                                <button type="submit" class="btn btn-info waves-effect waves-light m-t-5"> <i class="fa fa-save m-r-5"></i> <span>Lưu</span> </button>
	                                        </form>
                        				</div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>


@endsection
@section('js')
<!--Wysiwig js-->
<script src="plugins/tinymce/tinymce.min.js"></script>
@endsection
@section('script')
<script>
@if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
$(document).ready(function () {
})
        </script>
</script>
@include('ckfinder::setup')
        <script src={{ url('ckeditor/ckeditor.js') }}></script>
    <script>
    CKEDITOR.replace( 'noidung', {
        filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',

    } );
    </script>
@endsection