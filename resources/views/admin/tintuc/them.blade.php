@extends('admin.layout.main')
@section('css')
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<!-- Sweet Alert -->
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Tin tức </h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                <div class="row">
                                    <h4 class="m-t-5 header-title"><b>Thêm tin tức</b></h4>
                                    </div>
                                    <div class="col-md-12">
                        					<form class="form-horizontal" method="POST" action="{{route('post_themtt')}}" enctype="multipart/form-data">
											@csrf
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">Tên tin tức</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" value="" name="ten_tintuc" required>
	                                                </div>
	                                            </div>
												<div class="form-group">
	                                                <label class="col-md-2 control-label">Mã tin tức</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" value="" name="ma_tin" required>
	                                                </div>
	                                            </div>
												<div class="form-group">
                                                <label class="col-md-2 control-label">Hình ảnh</label>
	                                                <div class="col-md-10">
                                                    <input type="file" required name="hinhanh" required />
	                                                </div>
                                                </div>
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">Nội dung</label>
	                                                <div class="col-md-10">
	                                                    <textarea class="form-control" id="noidung" name="noidung"></textarea>
	                                                </div>
	                                            </div>
												<div class="form-group">
	                                                <label class="col-sm-2 control-label">Tình trạng</label>
	                                                <div class="col-sm-10">
	                                                    <select class="form-control" name="tinhtrang">
	                                                        <option value="1">Hiện</option>
	                                                        <option value="0">Ẩn</option>
	                                                        
	                                                    </select>
	                                                </div>
	                                            </div>
                                                <button type="submit" class="btn btn-info waves-effect waves-light m-t-5"> <i class="fa fa-save m-r-5"></i> <span>Lưu</span> </button>
	                                        </form>
                        				</div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>


@endsection
@section('js')

@endsection
@section('script')
<script>
@if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
$(document).ready(function () {
    
});
        </script>
@include('ckfinder::setup')
        <script src={{ url('ckeditor/ckeditor.js') }}></script>
    <script>
    CKEDITOR.replace( 'noidung', {
        filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',

    } );
    </script>
@endsection