@extends('frontend.layout.main')
@section('css')

@endsection
@section('content')
<div class="container mt-3">
<nav class="breadcrumb">
    <a class="breadcrumb-item" href="#">Trang chủ</a>
    <span class="breadcrumb-item active">Chi tiết đơn hàng</span>
</nav>
<div class="wrapper d-flex">
    <div class="col-lg-12">
    <h3>Thông tin chi tiết đơn hàng</h3>
        <div class="row flex-column mt-5">
            <strong>Trạng thái: @if($donhang->trangthai === 1) <span class="label text-danger">Chưa giao</span>@elseif($donhang->trangthai === 2)<span class="label text-success">Đã giao</span>@else<span class="label text-secondary">Đã hủy</span>@endif</strong>
            <span class="text-dark">Mã đơn hàng: {{$donhang->id_donhang}}</span>
            <span class="text-dark">Ngày tạo: @datetime($donhang->created_at)</span>
            <span class="text-dark">Cập nhật: @datetime($donhang->updated_at)</span>
        </div>
            <div class="row mb-5">
            <table id="cart" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th style="width:50%">Sản phẩm</th>
            <th style="width:10%">Giá</th>
            <th style="width:8%">Số lượng</th>
            <th style="width:15%" class="text-center">Tổng</th>
            <th style="width:17%"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($chitietdonhang as $ct)
            <tr>
                <td data-th="Product">
                    <div class="row">
                        <div class="col-sm-3 hidden-xs"><img src="assets/images/sanpham/{{ $ct->hinhanh }}" width="100" class="img-responsive"/></div>
                        <div class="col-sm-9">
                            <h4 class="nomargin">{{ $ct->ten_sanpham }}</h4>
                        </div>
                    </div>
                </td>
                <td>@money($ct->giadexuat - $ct->giamgia)</td>
                <td>{{$ct->soluong}}</td>
                <td class="text-center">@money(($ct->giadexuat - $ct->giamgia) * $ct->soluong)</td>
                <td class="actions"></td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="4" class="hidden-xs"></td>
            <td class="hidden-xs text-center"><strong>Tổng @money($total)</strong></td>
        </tr>
        <tr>
            <td></td>
            <td class="hidden-xs"></td>
            <td colspan="3">@if($donhang->trangthai !== 4 && $donhang->trangthai !== 5)<a href="donhang/{{$donhang->id_donhang}}/huy" class="btn btn-danger dathang">Hủy</a>@endif</td>
        </tr>
        </tfoot>
    </table>
            </div>
            
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
</script>
@endsection