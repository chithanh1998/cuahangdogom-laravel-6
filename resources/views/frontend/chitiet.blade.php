@extends('frontend.layout.main')
@section('css')

@endsection
@section('content')
<div class="container mt-3">
<nav class="breadcrumb">
    <a class="breadcrumb-item" href="#">Trang chủ</a>
    <a class="breadcrumb-item" href="danhmuc/{{$danhmuc->id_loaisanpham}}">{{$danhmuc->ten_loaisanpham}}</a>
    <span class="breadcrumb-item active">{{$sp->ten_sanpham}}</span>
</nav>
<div class="wrapper d-flex">
    <div class="col-lg-9">
            <div class="row">
                <div class="col-lg-6">
                    <div class="product-image d-flex align-items-center justify-content-center">
                        <img src="assets/images/sanpham/{{$sp->hinhanh}}" alt="">
                    </div>
                </div>
                <div class="col-lg-6 d-flex flex-column">
                    <form action="giohang/them/{{$sp->id_sanpham}}" method="post">
                    @csrf
                    <h2>{{$sp->ten_sanpham}}</h2>
                    @if($sp->giamgia > 0)
                    <h3 class="price special">@money($sp->giadexuat - $sp->giamgia)</h3>
                    <h3 class="price old">@money($sp->giadexuat)</h3>
                    @else
                    <h3 class="price">@money($sp->giadexuat)</h3>
                    @endif
                    <div class="qty">Số lượng: {{$sp->soluong}}</div>
                    <div class="status">Trạng thái: @if($sp->tinhtrang === 1)Trong kho @else Hết hàng @endif</div>
                    
                    @if($sp->soluong !== 0 && $sp->tinhtrang !== 0)
                    <div class="cart-qty">
                        <label>Số lượng:</label>
					    <input name="qty" type="number" min="1" class="product_qty"  value="1" />
                    </div>
                    <button type="submit" class="btn btn-danger">Thêm vào giỏ hàng</button>
                    @endif
                    </form>
                </div>
            </div>
            <div class="row description">
                <div class="row product-tab-row">
                    <ul class="product__tab d-flex align-items-center">
                        <li class="product__tab-item product__tab-item--active">Mô tả sản phẩm</li>

                    </ul>
                </div>
                <div class="product-description">
                {!!$sp->noidung!!}
                </div>
                
            </div>
        </div>
        <div class="col-lg-3 right-side">
            <div class="row">
                <div class="new-products">
                <div class="row product-tab-row">
                <ul class="product__tab d-flex align-items-center">
                    <li class="product__tab-item product__tab-item--active">Sản phẩm mới</li>

                </ul>
                </div>
                <div class="row product-item-row ">
                    @foreach($sanphammoi as $sp)
                        <a href="sanpham/{{$sp->ma_sanpham}}" class="product__item d-flex">
                            <div class="product__item-image">
                                <img src="assets/images/sanpham/{{$sp->hinhanh}}" alt=""class="">
                            </div>
                            <div class="product__item-name-price d-flex flex-column">
                                <h4 class="product__item-title">{{$sp->ten_sanpham}}</h4>
                                <p class="product__item-price">@money($sp->giadexuat)</p>
                            </div>
                        </a>
                    @endforeach
                </div>
                </div>
            </div>
            <div class="row">
                <div class="popular-products">
                <div class="row product-tab-row">
                <ul class="product__tab d-flex align-items-center">
                    <li class="product__tab-item product__tab-item--active">Sẩn phẩm bán chạy</li>
                </ul>
                </div>
                <div class="row product-item-row">
                    @foreach($banchay as $bc)
                    @foreach($sanpham as $sp)
                    @if($bc->id_sanpham === $sp->id_sanpham)
                        <a href="sanpham/{{$sp->ma_sanpham}}" class="product__item d-flex">
                            <div class="product__item-image">
                                <img src="assets/images/sanpham/{{$sp->hinhanh}}" alt=""class="">
                            </div>
                            <div class="product__item-name-price d-flex flex-column">
                                <h4 class="product__item-title">{{$sp->ten_sanpham}}</h4>
                                <p class="product__item-price">@money($sp->giadexuat)</p>
                            </div>
                        </a>
                    @endif
                    @endforeach
                    @endforeach
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
</script>
@endsection