@extends('frontend.layout.main')
@section('css')

@endsection
@section('content')
<div class="container mt-3">
<nav class="breadcrumb">
    <a class="breadcrumb-item" href="#">Trang chủ</a>
    <span class="breadcrumb-item active">Trang cá nhân</span>
</nav>
<div class="wrapper d-flex">
    <div class="col-lg-12">
    <h3>Trang cá nhân</h3>
      <div class="row product-tab-row">
          <ul class="product__tab d-flex align-items-center">
              <li class="product__tab-item product__tab-item--active">Thông tin cá nhân</li>
              <li class="product__tab-item product__tab-item"><a href="donhang">Đơn hàng</a></li>
          </ul>
      </div>
      <div class="row">
        <form class="w-100 mb-5" method="POST" action="trangcanhan/luu">
        @csrf
            <div class="form-group">
              <label for="ten_khachhang">Họ tên</label>
              <input type="text" class="form-control" name="ten_khachhang" placeholder="Họ tên" value="{{$khachhang->ten_khachhang}}" required>
            </div>
            <div class="form-group ">
              <label for="diachi">Địa chỉ</label>
              <input type="text" class="form-control" name="diachi" placeholder="Địa chỉ" value="{{$khachhang->diachi}}" required>
            </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" placeholder="Email" value="{{$khachhang->email}}" required>
          </div>
          <div class="form-group">
            <label for="dienthoai">Điện thoại</label>
            <input type="number" class="form-control" name="dienthoai" placeholder="Số điện thoại" value="{{$khachhang->dienthoai}}" required>
          </div>
          <button type="submit" class="btn btn-dark">Lưu</button>
          <button type="button" class="btn btn-danger dangxuat"><a href="dangxuat">Đăng xuất</a></button>
        </form>
      </div>
    </div>
        
    </div>
</div>
@endsection
@section('script')
<script>
</script>
@endsection