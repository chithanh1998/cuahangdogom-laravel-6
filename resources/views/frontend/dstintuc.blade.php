@extends('frontend.layout.main')
@section('css')

@endsection
@section('content')
<div class="container">
            <div class="row category-slide">
                <div class="col-lg-3 category">
                    <ul class="category__list d-flex flex-column">
                        <li class="category__item d-flex align-items-center">
                            <i class="fa fa-bars"></i>
                            <span href="#" class="category__item-link">Danh mục</span>
                        </li>
                        @foreach($danhmuc as $dm)
                        <li class="category__item d-flex justify-content-between align-items-center">
                            <a href="danhmuc/{{$dm->id_loaisanpham}}" class="category__item-link">{{$dm->ten_loaisanpham}}</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-lg-9">
                    <div class="banner">
                        @foreach($banner as $bn)
                        <img src="assets/images/banner/{{$bn->hinhanh}}" alt="">
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
<div class="">
    <div class="container">
        <div class="row product-tab-row">
            <ul class="product__tab d-flex align-items-center">
                <li class="product__tab-item product__tab-item--active">Tin tức</li>
                
            </ul>
        </div>
        @foreach($tintuc as $tt)
        <div class="row news-item">
            <a href="tintuc/{{$tt->ma_tin}}" class="col-lg-3">
                <img src="assets/images/tintuc/{{$tt->hinhanh}}" alt="">
            </a>
            <a href="tintuc/{{$tt->ma_tin}}" class="col-lg-9">
                <h4>{{$tt->ten_tintuc}}</h4>
                <span>Ngày đăng: @datetime($tt->created_at)</span>
            </a>
        </div>
        @endforeach
        {{ $tintuc->links() }}
    </div>
</div>
@endsection
@section('script')

@endsection