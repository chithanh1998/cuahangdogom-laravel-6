<header class="header">
        <div class="header__bar">
            <div class="container">
                <div class="row header__top justify-content-between">
                    <a href="#" class="header__logo col-lg-3 col-sm-9 col-6 d-flex align-items-center">
                        <img src="assets/images/logo/{{$caidat->logo}}" alt="" height="50" />
                    </a>
                    <nav class="menu col-lg-7 d-flex">
                        <ul class="menu__list d-flex align-items-center">
                            <i class="fa fa-times d-lg-none menu__close"></i>
                            <li class="menu__item">
                                <a href="#" class="menu__item-link">Trang chủ</a>
                            </li>
                            <li class="menu__item">
                                <a href="#" class="menu__item-link">Sản phẩm</a>
                            </li>
                            <li class="menu__item">
                                <a href="{{route('tintuc')}}" class="menu__item-link">Tin tức</a>
                            </li>
                            <li class="menu__item">
                                <a href="phanhoi" class="menu__item-link">Phản hồi</a>
                            </li>
                        </ul>
                    </nav>
                    <div class="header__icon col-lg-2 col-sm-3 col-6 d-flex justify-content-between align-items-center">
                        <a>                        
                            <i class="fas fa-search header-icon" id="search"></i>
                        </a>
                        <a href="giohang">                        
                            <i class="fas fa-shopping-basket header-icon"> {{ count((array) session('cart')) }}</i>
                        </a>
                        <a href="dangnhap">
                        <i class="fas fa-user header-icon"></i>
                        </a>
                        <i class="fas fa-bars header-icon d-lg-none menu-toggle"></i>
                    </div>
                    
                </div>
            </div>
        </div>
    </header>