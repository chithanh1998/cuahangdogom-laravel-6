<!DOCTYPE html>
<html lang="en">
<head>
    <base href="{{asset('')}}">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,600">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css"/>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet/less" type="text/css" href="less/style.less">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" href="css/slick-theme.css">
    <link href="plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css">
    @yield('css')
    <title>{{$caidat->tencuahang}}</title>
</head>
<body>
    <!-- ***** Header ***** -->
    @include('frontend.layout.header')
    <!-- ***** Search box ***** -->
    <div class="row justify-content-center search mt-3 d-none">
                        <div class="col-12 col-md-10 col-lg-8">
                            <form class="card card-sm" method="POST" action="timkiem">
                                @csrf
                                <div class="card-body row no-gutters align-items-center">
                                    <div class="col-auto">
                                        <i class="fas fa-search h4 "></i>
                                    </div>
                                    <!--end of col-->
                                    <div class="col">
                                        <input class="form-control form-control-lg form-control-borderless" type="search" placeholder="Nhập tên sản phẩm" name="timkiem" required id="timkiem">
                                    </div>
                                    <!--end of col-->
                                    <!--end of col-->
                                </div>
                            </form>

                            <div class="result-ajax"></div>
                        </div>
                        <!--end of col-->
                    </div>
    @yield('content')
    
    <!-- ***** Footer ***** -->
    @include('frontend.layout.footer')

    
</body>

<script src="js/less.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="plugins/toastr/toastr.min.js"></script>
@yield('script')
<script>
    @if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
    $(document).ready(function(){
        $('.menu-toggle').click(function(){
            $('.menu').addClass('menu__open');
        })
        $('.menu__close').click(function(){
            $('.menu').removeClass('menu__open');
        })
        $('#search').click(function(){
            $('.search').removeClass('d-none');
        })
        $(document).mouseup(function(e){
            var container = $('.menu');
            if(!container.is(e.target) && container.has(e.target).length === 0)
            {
                container.removeClass('menu__open');
            }
        })
        $('.banner').slick({
            autoplay: true,
            
        });

        $('#timkiem').keyup(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var timkiem = $(this).val();
            if(timkiem != '') {
            $.ajax({
                url:"{{ route('timkiemajax') }}",
                method: "POST",
                data: {timkiem:timkiem},
                success: function(resp){ 
                $('.result-ajax').show();  
                $('.result-ajax').html(resp);
                }
            });
            } else {
                $('.result-ajax').hide();
            }
        });
    })
</script>
</html>