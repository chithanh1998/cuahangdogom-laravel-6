<div class="footer">
        <div class="container">
            <div class="row footer__bot">
                <div class="col-lg-4 col-sm-6">
                    <div class="footer__bot-title">
                        Liên hệ với chúng tôi
                    </div>
                    <div class="footer__bot-item d-flex align-items-center">
                        <i class="fas fa-map-marker-alt"></i>
                        <p>{{$caidat->diachi}}</p>
                    </div>
                    <div class="footer__bot-item d-flex align-items-center">
                        <i class="fas fa-phone"></i>
                        <a href="tel:{{$caidat->dienthoai}}">{{$caidat->dienthoai}}</a>
                    </div>
                    <div class="footer__bot-item d-flex align-items-center">
                        <i class="fas fa-envelope"></i>
                        <a href="mailto:{{$caidat->email}}">{{$caidat->email}}</a>
                    </div>
                    <div class="footer__bot-item d-flex align-items-center">
                        <i class="fas fa-clock"></i>
                        <p>9:00 - 20:00 Tất cả các ngày</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 pt-3 pt-sm-0">
                    <div class="footer__bot-title">
                        Tài khoản
                    </div>
                    <div class="footer__bot-item d-flex align-items-center">
                        <a href="#">Tài khoản của tôi</a>
                    </div>
                    <div class="footer__bot-item d-flex align-items-center">
                        <a href="#">Đăng ký</a>
                    </div>
                    <div class="footer__bot-item d-flex align-items-center">
                        <a href="#">Đăng nhập</a>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 pt-3 pt-lg-0">
                    <div class="footer__bot-title">
                        Thông tin
                    </div>
                    <div class="footer__bot-item d-flex align-items-center">
                        <a href="#">Về chúng tôi</a>
                    </div>
                    <div class="footer__bot-item d-flex align-items-center">
                        <a href="#">Liên hệ</a>
                    </div>
                    <div class="footer__bot-item d-flex align-items-center">
                        <a href="#">Điều khoản</a>
                    </div>
                    <div class="footer__bot-item d-flex align-items-center">
                        <a href="#">Chính sách</a>
                    </div>
                </div>
            </div>
        </div>
    </div>