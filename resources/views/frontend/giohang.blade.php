@extends('frontend.layout.main')
@section('css')

@endsection
@section('content')
<div class="container mt-3">
<nav class="breadcrumb">
    <a class="breadcrumb-item" href="#">Trang chủ</a>
    <span class="breadcrumb-item active">Giỏ hàng</span>
</nav>
<div class="wrapper d-flex">
    <div class="col-lg-9">
            <div class="row">
            <table id="cart" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th style="width:50%">Sản phẩm</th>
            <th style="width:10%">Giá</th>
            <th style="width:8%">Số lượng</th>
            <th style="width:22%" class="text-center">Tổng</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
        <?php $total = 0 ?>
        @if(session('cart'))
            @foreach(session('cart') as $id => $details)
                <?php $total += $details['price'] * $details['quantity'] ?>
                <tr>
                    <td data-th="Product">
                        <div class="row">
                            <div class="col-sm-3 hidden-xs"><img src="assets/images/sanpham/{{ $details['photo'] }}" width="100" class="img-responsive"/></div>
                            <div class="col-sm-9">
                                <h4 class="nomargin">{{ $details['name'] }}</h4>
                            </div>
                        </div>
                    </td>
                    <td data-th="Price">@money($details['price'])</td>
                    <td data-th="Quantity">
                        <input type="number" value="{{ $details['quantity'] }}" class="form-control quantity" />
                    </td>
                    <td data-th="Subtotal" class="text-center">@money($details['price'] * $details['quantity'])</td>
                    <td class="actions" data-th="">
                        <button class="btn btn-info btn-sm update-cart" data-id="{{ $id }}"><i class="fas fa-sync-alt"></i></button>
                        <button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fas fa-trash"></i></button>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
        <tfoot>
        <tr>
            <td colspan="4" class="hidden-xs"></td>
            <td class="hidden-xs text-center"><strong>Tổng @money($total)</strong></td>
        </tr>
        <tr>
            <td><a href="{{ url('/') }}" class="btn btn-secondary"><i class="fa fa-angle-left"></i> Tiếp tục mua sắm</a></td>
            <td class="hidden-xs"></td>
            <td colspan="3">@if(session('cart'))<a href="{{ url('dathang') }}" class="btn btn-primary dathang">Đặt hàng <i class="fa fa-angle-right"></i></a>@endif</td>
        </tr>
        </tfoot>
    </table>
            </div>
            
        </div>
        <div class="col-lg-3 right-side">
            <div class="row">
                <div class="new-products">
                <div class="row product-tab-row">
                <ul class="product__tab d-flex align-items-center">
                    <li class="product__tab-item product__tab-item--active">Sản phẩm mới</li>

                </ul>
                </div>
                <div class="row product-item-row ">
                    @foreach($sanphammoi as $sp)
                        <a href="sanpham/{{$sp->ma_sanpham}}" class="product__item d-flex">
                            <div class="product__item-image">
                                <img src="assets/images/sanpham/{{$sp->hinhanh}}" alt=""class="">
                            </div>
                            <div class="product__item-name-price d-flex flex-column">
                                <h4 class="product__item-title">{{$sp->ten_sanpham}}</h4>
                                <p class="product__item-price">@money($sp->giadexuat)</p>
                            </div>
                        </a>
                    @endforeach
                </div>
                </div>
            </div>
            <div class="row">
                <div class="popular-products">
                <div class="row product-tab-row">
                <ul class="product__tab d-flex align-items-center">
                    <li class="product__tab-item product__tab-item--active">Sẩn phẩm bán chạy</li>
                </ul>
                </div>
                <div class="row product-item-row">
                    @foreach($banchay as $bc)
                    @foreach($sanpham as $sp)
                    @if($bc->id_sanpham === $sp->id_sanpham)
                        <a href="sanpham/{{$sp->ma_sanpham}}" class="product__item d-flex">
                            <div class="product__item-image">
                                <img src="assets/images/sanpham/{{$sp->hinhanh}}" alt=""class="">
                            </div>
                            <div class="product__item-name-price d-flex flex-column">
                                <h4 class="product__item-title">{{$sp->ten_sanpham}}</h4>
                                <p class="product__item-price">@money($sp->giadexuat)</p>
                            </div>
                        </a>
                    @endif
                    @endforeach
                    @endforeach
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(".update-cart").click(function (e) {
           e.preventDefault();
           var ele = $(this);
            $.ajax({
               url: '{{ url('update-cart') }}',
               method: "patch",
               data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
               success: function (response) {
                   window.location.reload();
               }
            });
        });
        $(".remove-from-cart").click(function (e) {
            e.preventDefault();
            var ele = $(this);
            if(confirm("Bạn chắc chắn muốn xóa")) {
                $.ajax({
                    url: '{{ url('remove-from-cart') }}',
                    method: "DELETE",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });
</script>
@endsection