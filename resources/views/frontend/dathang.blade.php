@extends('frontend.layout.main')
@section('css')

@endsection
@section('content')
<div class="container mt-3">
<nav class="breadcrumb">
    <a class="breadcrumb-item" href="#">Trang chủ</a>
    <span class="breadcrumb-item active">Đặt hàng</span>
</nav>
<div class="wrapper d-flex">
    <div class="col-lg-12">
    <h3>Thông tin chi tiết đơn hàng</h3>
        <div class="row flex-column">
            <strong>{{$khachhang->ten_khachhang}}</strong>
            <span class="text-dark">Địa chỉ: {{$khachhang->diachi}}</span>
            <span class="text-dark">Email: {{$khachhang->email}}</span>
            <span class="text-dark">Điện thoại: {{$khachhang->dienthoai}}</span>
        </div>
            <div class="row">
            <table id="cart" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th style="width:50%">Sản phẩm</th>
            <th style="width:10%">Giá</th>
            <th style="width:8%">Số lượng</th>
            <th style="width:22%" class="text-center">Tổng</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
        <?php $total = 0 ?>
        @if(session('cart'))
            @foreach(session('cart') as $id => $details)
                <?php $total += $details['price'] * $details['quantity'] ?>
                <tr>
                    <td data-th="Product">
                        <div class="row">
                            <div class="col-sm-3 hidden-xs"><img src="assets/images/sanpham/{{ $details['photo'] }}" width="100" class="img-responsive"/></div>
                            <div class="col-sm-9">
                                <h4 class="nomargin">{{ $details['name'] }}</h4>
                            </div>
                        </div>
                    </td>
                    <td data-th="Price">@money($details['price'])</td>
                    <td data-th="Quantity">
                        {{ $details['quantity'] }}
                    </td>
                    <td data-th="Subtotal" class="text-center">@money($details['price'] * $details['quantity'])</td>
                    <td class="actions" data-th="">
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
        <tfoot>
        <tr>
            <td colspan="4" class="hidden-xs"></td>
            <td class="hidden-xs text-center"><strong>Tổng @money($total)</strong></td>
        </tr>
        <tr>
            <td></td>
            <td class="hidden-xs"></td>
            <td colspan="3">@if(session('cart'))<a href="{{ url('dathang/dathang') }}" class="btn btn-success dathang">Đặt hàng <i class="fa fa-angle-right"></i></a>@endif</td>
        </tr>
        </tfoot>
    </table>
            </div>
            
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
</script>
@endsection