@extends('frontend.layout.main')
@section('css')

@endsection
@section('content')
<div class="container mt-3">
<nav class="breadcrumb">
    <a class="breadcrumb-item" href="#">Trang chủ</a>
    <a class="breadcrumb-item" href="tintuc">Tin tức</a>
    <span class="breadcrumb-item active">{{$tintuc->ten_tintuc}}</span>
</nav>
<div class="wrapper d-flex">
    <div class="col-lg-9">
            <div class="row flex-column news-head">
                <h1>{{$tintuc->ten_tintuc}}</h1>
                <small>Ngày đăng: @datetime($tintuc->created_at)</small>
                <img src="assets/images/tintuc/{{$tintuc->hinhanh}}" alt="">
            </div>
            <div class="row news-content">               
                {!!$tintuc->noidung!!}             
            </div>
        </div>
        <div class="col-lg-3 right-side">
            <div class="row">
                <div class="new-news">
                <div class="row product-tab-row">
                <ul class="product__tab d-flex align-items-center">
                    <li class="product__tab-item product__tab-item--active">Tin tức mới</li>

                </ul>
                </div>
                <div class="row product-item-row ">
                    @foreach($tintucmoi as $tm)
                        <a href="tintuc/{{$tm->ma_tin}}" class=" d-flex">
                            <div class="product__item-name-price d-flex flex-column">
                                <span class="product__item-title">{{$tm->ten_tintuc}}</span>
                            </div>
                        </a>
                    @endforeach
                </div>
                </div>
            </div>
        </div>
</div>
@endsection
@section('script')
<script>
</script>
@endsection