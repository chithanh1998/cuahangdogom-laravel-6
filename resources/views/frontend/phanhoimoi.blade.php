@extends('frontend.layout.main')
@section('css')

@endsection
@section('content')
<div class="container mt-3">
<nav class="breadcrumb">
    <a class="breadcrumb-item" href="#">Trang chủ</a>
    <a class="breadcrumb-item" href="phanhoi">Phản hồi</a>
    <span class="breadcrumb-item active">Phản hồi mới</span>
</nav>
<div class="wrapper d-flex">
    <div class="col-lg-12">
    <h3>Phản hồi mới</h3>
      <div class="row product-tab-row">
          <ul class="product__tab d-flex align-items-center">
              <li class="product__tab-item product__tab-item--active">Phản hồi mới</li>
          </ul>
      </div>
      <div class="row">
        <form class="w-100 mb-5" method="POST" action="phanhoi/tao">
        @csrf
            <div class="form-group">
              <label for="ten_khachhang">Chủ đề</label>
              <input type="text" class="form-control" name="chude" placeholder="" required>
            </div>
            <div class="form-group">
              <label for="noidung">Nội dung</label>
              <textarea class="form-control" name="noidung" id="noidung" rows="5"></textarea>
            </div>
          <button type="submit" class="btn btn-dark">Gửi</button>
        </form>
      </div>
    </div>
    </div>
</div>
@endsection
@section('script')
<script>
</script>
@endsection