@extends('frontend.layout.main')
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
@endsection
@section('content')
<div class="container mt-3">
<nav class="breadcrumb">
    <a class="breadcrumb-item" href="#">Trang chủ</a>
    <span class="breadcrumb-item active">Đơn hàng</span>
</nav>
<div class="wrapper d-flex">
    <div class="col-lg-12">
    <h3>Danh sách đơn hàng</h3>
      <div class="row product-tab-row">
          <ul class="product__tab d-flex align-items-center">
              <li class="product__tab-item product__tab-item"><a href="trangcanhan">Thông tin cá nhân</a></li>
              <li class="product__tab-item product__tab-item--active">Đơn hàng</li>
          </ul>
      </div>
      <div class="row mb-5">
        <table class="table" id="datatable">
            <thead>
                <tr>
                    <th>Mã đơn hàng</th>
                    <th>Ngày đặt</th>
                    <th>Cập nhật</th>
                    <th>Trạng thái</th>
                    <th>Chi tiết</th>
                </tr>
            </thead>
            <tbody>
                @foreach($donhang as $dh)
                <tr>
                    <td>{{$dh->id_donhang}}</td>
                    <td>@datetime($dh->created_at)</td>
                    <td>@datetime($dh->updated_at)</td>
                    @if($dh->trangthai === 1) 
                    <td>Chưa giao</td>
                    @elseif($dh->trangthai === 2)
                    <td>Đã giao</td>
                    @else
                    <td>Đã hủy</td>
                    @endif
                    <td>
                    <a href="donhang/chitiet/{{$dh->id_donhang}}"  class="chitiet"><i class="fa fa-search"></i>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
      </div>
    </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script>
			$.extend( true, $.fn.dataTable.defaults, {
			    "language": {
            "lengthMenu": "Hiển thị _MENU_ giá trị",
            "zeroRecords": "Không tìm thấy giá trị nào",
            "info": "Hiển thị trang _PAGE_ trên _PAGES_",
            "infoEmpty": "Không có giá trị nào",
            "infoFiltered": "(Lọc từ _MAX_ giá trị)",
            "search": "Tìm kiếm:",
            "paginate": {
                "first":      "Đầu",
                "last":       "Cuối",
                "next":       "Tiếp",
                "previous":   "Trước"
            },
        },
			} );
		</script>
<script>
$(document).ready(function () {
    $('#datatable').DataTable( {
        "order": []
    } );
});
</script>
@endsection