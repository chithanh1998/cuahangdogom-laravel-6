@extends('frontend.layout.main')
@section('css')

@endsection
@section('content')
<div class="container mt-3">
<nav class="breadcrumb">
    <a class="breadcrumb-item" href="#">Trang chủ</a>
    <span class="breadcrumb-item active">Đăng ký</span>
</nav>
<div class="wrapper d-flex justify-content-center">
    <div class="col-lg-6">
            <div class="row flex-column login-form">
                <div class="card mb-4">
            <article class="card-body">
                <h4 class="card-title text-center mb-4 mt-1">Đăng ký</h4>
                <hr>
                <p class="text-danger text-center">Hãy nhập tất cả các trường</p>
                <form>
                <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="ten_taikhoan" class="form-control" placeholder="Tên tài khoản" type="text" id="tentaikhoan">
                </div> <!-- input-group.// -->
                </div> <!-- form-group// -->
                <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                    </div>
                    <input class="form-control" placeholder="Mật khẩu" type="password" name="matkhau" id="matkhau">
                </div> <!-- input-group.// -->
                </div> <!-- form-group// -->
                <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="ten_khachhang" class="form-control" placeholder="Họ tên" type="text" id="ten_khachhang">
                </div> <!-- input-group.// -->
                </div> <!-- form-group// -->
                <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fas fa-map-marker-alt"></i> </span>
                    </div>
                    <input name="diachi" class="form-control" placeholder="Địa chỉ" type="text" id="diachi">
                </div> <!-- input-group.// -->
                </div> <!-- form-group// -->
                <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fas fa-envelope-open"></i> </span>
                    </div>
                    <input name="email" class="form-control" placeholder="Email" type="email" id="email">
                </div> <!-- input-group.// -->
                </div> <!-- form-group// -->
                <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fas fa-mobile-alt"></i> </span>
                    </div>
                    <input name="dienthoai" class="form-control" placeholder="Số điện thoại" type="number" id="dienthoai">
                </div> <!-- input-group.// -->
                </div> <!-- form-group// -->
                <div class="form-group">
                <button type="button" class="btn btn-dark btn-block" id="dangky"><div class="spinner-border text-light" role="status"></div> Đăng ký  </button>
                </div> <!-- form-group// -->
                </form>
            </article>
            </div> <!-- card.// -->
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
// Enter đăng nhập
$(document).bind('keydown', function(e){         
    if (e.which == 13){
       $('#dangky').trigger('click');   
    }     
        });
        $('#dangky').click(function(){
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var tentaikhoan = $('#tentaikhoan').val();
        var matkhau = $('#matkhau').val();
        var hoten = $('#ten_khachhang').val();
        var diachi = $('#diachi').val();
        var email = $('#email').val();
        var dienthoai = $('#dienthoai').val();

        $.ajax({
            type: 'post',
            url: '{{route("dangkyfront")}}',
            data: {tentaikhoan:tentaikhoan, matkhau:matkhau, ten_khachhang: hoten, diachi: diachi, email: email, dienthoai: dienthoai},
            beforeSend: function(){
                if(tentaikhoan == "" || matkhau == "" || hoten == "" || diachi == "" || email == "" || dienthoai == ""){
                    toastr["info"]("Hãy nhập tất cả các trường ");
                    return false;
                }
                $('.spinner-border').fadeIn();
                $("#dangky").attr("disabled", true);
            },
            success: function(resp){
				if(resp == "ok"){
                    toastr["success"]("Đăng ký thành công");
                    setTimeout('window.location.href = "dangnhap";',1000);
                }
                if(resp == "err"){
                    $('.spinner-border').fadeOut();
                    $("#dangky").removeAttr("disabled");
                    toastr["info"]("Tên tài khoản đã tồn tại");
                }
            } 
        })
        })
</script>
@endsection