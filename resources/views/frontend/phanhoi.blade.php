@extends('frontend.layout.main')
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
@endsection
@section('content')
<div class="container mt-3">
<nav class="breadcrumb">
    <a class="breadcrumb-item" href="#">Trang chủ</a>
    <span class="breadcrumb-item active">Phản hồi</span>
</nav>
<div class="wrapper d-flex">
    <div class="col-lg-12">
    <h3>Phản hồi của bạn</h3>
      <div class="row product-tab-row">
          <ul class="product__tab d-flex align-items-center">
              <li class="product__tab-item product__tab-item--active">Danh sách phản hồi</li>
          </ul>
      </div>
      <div class="row w-100 justify-content-end">
      <button type="button" class="btn btn-dark phanhoi mb-5"><a href="phanhoi/tao">Tạo phản hồi</a></button>
      </div>
      <div class="row mb-5">
        <table class="table" id="datatable">
            <thead>
                <tr>
                    <th>Chủ đề</th>
                    <th>Ngày tạo</th>
                    <th>Cập nhật</th>
                    <th>Chi tiết</th>
                </tr>
            </thead>
            <tbody>
                @foreach($phanhoi as $ph)
                <tr>
                    <td>{{$ph->chude}}</td>
                    <td>@datetime($ph->created_at)</td>
                    <td>@datetime($ph->updated_at)</td>
                    <td>
                    <a href="phanhoi/chitiet/{{$ph->id_topic}}" class="chitiet"><i class="fa fa-search"></i>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
      </div>
    </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script>
			$.extend( true, $.fn.dataTable.defaults, {
			    "language": {
            "lengthMenu": "Hiển thị _MENU_ giá trị",
            "zeroRecords": "Không tìm thấy giá trị nào",
            "info": "Hiển thị trang _PAGE_ trên _PAGES_",
            "infoEmpty": "Không có giá trị nào",
            "infoFiltered": "(Lọc từ _MAX_ giá trị)",
            "search": "Tìm kiếm:",
            "paginate": {
                "first":      "Đầu",
                "last":       "Cuối",
                "next":       "Tiếp",
                "previous":   "Trước"
            },
        },
			} );
		</script>
<script>
$(document).ready(function () {
    $('#datatable').DataTable( {
        "order": []
    } );
});
</script>
@endsection