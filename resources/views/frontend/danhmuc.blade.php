@extends('frontend.layout.main')
@section('css')

@endsection
@section('content')
<div class="container">
            <div class="row category-slide">
                <div class="col-lg-3 category">
                    <ul class="category__list d-flex flex-column">
                        <li class="category__item d-flex align-items-center">
                            <i class="fa fa-bars"></i>
                            <span href="#" class="category__item-link">Danh mục</span>
                        </li>
                        @foreach($danhmuc as $dm)
                        <li class="category__item d-flex justify-content-between align-items-center">
                            <a href="danhmuc/{{$dm->id_loaisanpham}}" class="category__item-link">{{$dm->ten_loaisanpham}}</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-lg-9">
                    <div class="banner">
                        @foreach($banner as $bn)
                        <img src="assets/images/banner/{{$bn->hinhanh}}" alt="">
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
<!-- ***** Product ***** -->
<div class="product">
    <div class="container">
        <div class="row product-tab-row">
            <ul class="product__tab d-flex align-items-center">
                <li class="product__tab-item product__tab-item--active">{{$tendanhmuc}}</li>
                
            </ul>
        </div>
        <div class="row product-item-row ">
        @foreach($sanpham as $sp)
            <a href="sanpham/{{$sp->ma_sanpham}}" class="col-lg-3 col-sm-6 product__item d-flex flex-column">
                <div class="product__item-image">
                    <img src="assets/images/sanpham/{{$sp->hinhanh}}" alt=""class="">
                </div>
                <h4 class="product__item-title">{{$sp->ten_sanpham}}</h4>
                @if($sp->giamgia > 0) <p class="product__item-price special">@money($sp->giadexuat - $sp->giamgia)</p> <p class="product__item-price old">@money($sp->giadexuat)</p> @else
                <p class="product__item-price">@money($sp->giadexuat)</p>
                @endif
            </a>
        @endforeach
        </div>
        {{ $sanpham->links() }}
    </div>
</div>
@endsection
@section('script')

@endsection