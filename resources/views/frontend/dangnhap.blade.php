@extends('frontend.layout.main')
@section('css')

@endsection
@section('content')
<div class="container mt-3">
<nav class="breadcrumb">
    <a class="breadcrumb-item" href="#">Trang chủ</a>
    <span class="breadcrumb-item active">Đăng nhập</span>
</nav>
<div class="wrapper d-flex justify-content-center">
    <div class="col-lg-6">
            <div class="row flex-column login-form">
                <div class="card mb-4">
            <article class="card-body">
                <h4 class="card-title text-center mb-4 mt-1">Đăng nhập</h4>
                <hr>
                <p class="text-success text-center">Hãy đăng nhập để tiếp tục mua sắm</p>
                <form>
                <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                    </div>
                    <input name="ten_taikhoan" class="form-control" placeholder="Tên tài khoản" type="text" id="tentaikhoan">
                </div> <!-- input-group.// -->
                </div> <!-- form-group// -->
                <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                    </div>
                    <input class="form-control" placeholder="Mật khẩu" type="password" name="matkhau" id="matkhau">
                </div> <!-- input-group.// -->
                </div> <!-- form-group// -->
                <div class="form-group">
                <button type="button" class="btn btn-dark btn-block" id="dangnhap"><div class="spinner-border text-light" role="status"></div> Đăng nhập  </button>
                </div> <!-- form-group// -->
                <p class="text-center "><a href="dangky" class="btn btn-secondary w-100">Chưa có tài khoản? Đăng ký</a></p>
                </form>
            </article>
            </div> <!-- card.// -->
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    // Enter đăng nhập
    $(document).bind('keydown', function(e){         
    if (e.which == 13){
       $('#dangnhap').trigger('click');   
    }     
        });
        $('#dangnhap').click(function(){
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var tentaikhoan = $('#tentaikhoan').val();
        var matkhau = $('#matkhau').val();
        $.ajax({
            type: 'post',
            url: '{{route("dangnhapfront")}}',
            data: {tentaikhoan:tentaikhoan, matkhau:matkhau},
            beforeSend: function(){
                if(tentaikhoan == "" || matkhau == ""){
                    toastr["info"]("Hãy nhập tên tài khoản và mật khẩu");
                    return false;
                }
                $('.spinner-border').fadeIn();
                $("#dangnhap").attr("disabled", true);
            },
            success: function(resp){
				if(resp == "customer"){
                    setTimeout('window.location.href = "/";',1000);
                }
                if(resp == "err"){
                    $('.spinner-border').fadeOut();
                    $("#dangnhap").removeAttr("disabled");
                    toastr["info"]("Tài khoản và mật khẩu không đúng");
                }
            } 
        })
        })
</script>
@endsection