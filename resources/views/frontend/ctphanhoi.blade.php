@extends('frontend.layout.main')
@section('css')

@endsection
@section('content')
<div class="container mt-3">
<nav class="breadcrumb">
    <a class="breadcrumb-item" href="#">Trang chủ</a>
    <a class="breadcrumb-item" href="phanhoi">Phản hồi</a>
    <span class="breadcrumb-item active">{{$phanhoi->chude}}</span>
</nav>
<div class="wrapper d-flex">
    <div class="col-lg-12">
    <h3>Chủ đề: {{$phanhoi->chude}}</h3>
      <div class="noidung-wrapper mt-3 mb-3 overflow-auto">
        @foreach($ctphanhoi as $ct)
        <div class="item mb-2 mt-2 flex-column">
        @if($ct->quyen === 1) <div class="ten text-danger"><strong>{{$ct->tentaikhoan}} (Admin)</strong> <small class="time">@datetime($ct->created_at)</small></div> <div class="noidung">{{$ct->noidung}}</div> @else 
        <div class="ten text-dark"><strong>{{$ct->tentaikhoan}}</strong> <small class="time">@datetime($ct->created_at)</small></div> <div class="noidung">{{$ct->noidung}}</div> @endif
        </div>
        @endforeach
      </div>
      <div class="row">
        <form class="w-100 mb-5" method="POST" action="phanhoi/chitiet/{{$phanhoi->id_topic}}/luu">
        @csrf
            <div class="form-group">
              <textarea class="form-control" name="noidung" placeholder="Nội dung" id="noidung" rows="5"></textarea>
            </div>
          <button type="submit" class="btn btn-dark">Gửi</button>
        </form>
      </div>
    </div>
    </div>
</div>
@endsection
@section('script')
<script>
</script>
@endsection