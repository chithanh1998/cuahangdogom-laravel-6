<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('thongtincanhan', 'TaikhoanController@get_thongtincanhan')->name('thongtincanhan');
// Route::post('thongtincanhan', 'TaikhoanController@post_thongtincanhan')->name('luuthongtincanhan');
// Route::post('thongtincanhan/doiavatar', 'TaikhoanController@doiavatar')->name('doiavatar');

Route::group(['prefix' => 'admin','middleware'=>['admin']], function () {
    Route::get('dangxuat', 'Admin\TaikhoanController@dangxuat')->name('dangxuat');
    Route::group(['prefix' => 'taikhoan'], function () {
        Route::get('/', 'Admin\TaikhoanController@ds')->name('dsqt');
        Route::post('them', 'Admin\TaikhoanController@post_them')->name('themqt');
        Route::get('thongtin', 'Admin\TaikhoanController@thongtin')->name('thongtinqt');
        Route::post('sua', 'Admin\TaikhoanController@post_sua')->name('luuchinhsuaqt');
        Route::get('xoa/{id}', 'Admin\TaikhoanController@xoa');
    });
    Route::group(['prefix' => 'sanpham'], function () {
        Route::get('/', 'Admin\SanphamController@ds')->name('dssp');
        Route::get('them', 'Admin\SanphamController@get_them')->name('pagethemsp');
        Route::post('them', 'Admin\SanphamController@post_them')->name('themsp');
        Route::get('sua/{id}', 'Admin\SanphamController@get_sua')->name('getsuasp');
        Route::post('sua', 'Admin\SanphamController@post_sua')->name('luuchinhsuasp');
        Route::get('xoa/{id}', 'Admin\SanphamController@xoa');
        
    });
    Route::group(['prefix' => 'loaisanpham'], function () {
        Route::get('/', 'Admin\SanphamController@dsloai')->name('dsloaisp');
        Route::post('them', 'Admin\SanphamController@post_themloai')->name('themloaisp');
        Route::post('sua', 'Admin\SanphamController@post_sualoai')->name('luuloaisp');
        Route::get('thongtin', 'Admin\SanphamController@get_thongtinloaisp')->name('thongtinloaisp');
        Route::get('xoa/{id}', 'Admin\SanphamController@xoaloai');
        
    });
    Route::group(['prefix' => 'khachhang'], function () {
        Route::get('/', 'Admin\KhachhangController@ds')->name('dskhachhang');
        Route::post('them', 'Admin\KhachhangController@post_them')->name('themkhachhang');
        Route::get('thongtin', 'Admin\KhachhangController@thongtin')->name('thongtinkhachhang');
        Route::post('sua', 'Admin\KhachhangController@post_sua')->name('luuchinhsuakhachhang');
        Route::get('xoa/{id}', 'Admin\KhachhangController@xoa');
    });
    Route::group(['prefix' => 'donhang'], function () {
        Route::get('/', 'Admin\DonhangController@ds')->name('dsdonhang');
        Route::post('them', 'Admin\DonhangController@post_them')->name('themdonhang');
        Route::get('chitiet/{id}', 'Admin\DonhangController@chitiet')->name('chitietdonhang');
        Route::get('chitiet/{id}/chuyentrangthai', 'Admin\DonhangController@chuyentrangthai');
        Route::get('chitiet/{id}/huy', 'Admin\DonhangController@huy');
        Route::post('sua', 'Admin\DonhangController@post_sua')->name('luuchinhsuadonhang');
        Route::get('xoa/{id}', 'Admin\DonhangController@xoa');
    });
    Route::group(['prefix' => 'tintuc'], function () {
        Route::get('/', 'Admin\TintucController@ds')->name('dstt');
        Route::get('them', 'Admin\TintucController@get_them')->name('get_themtt');
        Route::post('them', 'Admin\TintucController@post_them')->name('post_themtt');
        Route::get('sua/{id}', 'Admin\TintucController@get_sua');
        Route::post('sua/{id}', 'Admin\TintucController@post_sua');
        Route::get('xoa/{id}', 'Admin\TintucController@xoa');
        
    });
    Route::group(['prefix' => 'phanhoi'], function () {
        Route::get('/', 'Admin\PhanhoiController@ds')->name('dsphanhoi');
        Route::get('chitiet/{id}', 'Admin\PhanhoiController@chitiet')->name('chitietphanhoi');
        Route::post('chitiet/{id}/luu', 'Admin\PhanhoiController@luuphanhoi')->name('luuphanhoi');
    });

    Route::group(['prefix' => 'caidat'], function () {
        Route::get('/', 'Admin\CaidatController@caidat')->name('dscaidat');
        Route::post('sua', 'Admin\CaidatController@luucaidat')->name('luuchinhsuacaidat');
        
    });
    Route::get('thongke', 'Admin\ThongkeController@thongke')->name('thongke');
    Route::get('banner/xoa/{id}', 'Admin\CaidatController@xoabanner');
});

Route::group(['prefix' => '/','middleware'=>['customer']], function () {
    Route::get('dathang', 'FrontendController@get_dathang');
    Route::get('dathang/dathang', 'FrontendController@dathang');
    Route::get('trangcanhan', 'FrontendController@trangcanhan');
    Route::post('trangcanhan/luu', 'FrontendController@luuttcanhan');
    Route::get('donhang', 'FrontendController@donhang');
    Route::get('donhang/chitiet/{id}', 'FrontendController@ctdonhang');
    Route::get('donhang/{id}/huy', 'FrontendController@huydonhang');
    Route::get('phanhoi', 'FrontendController@dsphanhoi');
    Route::get('phanhoi/tao', 'FrontendController@get_taophanhoi');
    Route::post('phanhoi/tao', 'FrontendController@post_taophanhoi');
    Route::get('phanhoi/chitiet/{id}', 'FrontendController@ctphanhoi');
    Route::post('phanhoi/chitiet/{id}/luu', 'FrontendController@luuphanhoi');

});

Route::get('admin', 'Admin\TaikhoanController@get_dangnhap');
Route::post('dangnhapadmin', 'Admin\TaikhoanController@post_dangnhap')->name('dangnhap');
Route::get('init', 'Admin\TaikhoanController@init_admin');
Route::get('/', 'FrontendController@trangchu')->name('trangchu');
Route::get('danhmuc/{id}', 'FrontendController@danhmuc');
Route::get('sanpham/{ma_sanpham}', 'FrontendController@chitietsp');
Route::post('giohang/them/{id}', 'CartController@addToCart');
Route::patch('update-cart', 'CartController@update');
Route::delete('remove-from-cart', 'CartController@remove');
Route::get('giohang', 'CartController@cart');
Route::get('tintuc', 'FrontendController@tintuc')->name('tintuc');
Route::get('tintuc/{code}', 'FrontendController@chitiettintuc');
Route::get('dangnhap', 'FrontendController@get_dangnhap');
Route::get('dangxuat', 'FrontendController@dangxuat');
Route::post('dangnhap', 'FrontendController@post_dangnhap')->name('dangnhapfront');
Route::get('dangky', 'FrontendController@get_dangky');
Route::post('dangky', 'FrontendController@post_dangky')->name('dangkyfront');
Route::post('timkiem', 'FrontendController@timkiem')->name('timkiem');
Route::post('timkiemajax', 'FrontendController@timkiemajax')->name('timkiemajax');


